package h.util;

public class IncompleteMatch extends HException {
    public IncompleteMatch(String message) {
        super(message);
    }
}
