

package h.util;

import h.checking.Attribute;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;
import scala.xml.Null;

public class GraphViewer implements ViewerListener {

    protected boolean loop;

    protected Graph graph;

    public GraphViewer(Graph graph) {
        if (graph == null) { throw new NullPointerException(); }
        this.graph = graph;
    }

    public void view() {
        // We do as usual to display a graph. This
        // connect the graph outputs to the viewer.
        // The viewer is a sink of the graph.
        Viewer viewer = graph.display();

        // The default action when closing the view is to quit
        // the program.
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);

        // We connect back the viewer to the graph,
        // the graph becomes a sink for the viewer.
        // We also install us as a viewer listener to
        // intercept the graphic events.
        ViewerPipe fromViewer = viewer.newViewerPipe();
        fromViewer.addViewerListener(this);
        fromViewer.addSink(graph);

        // Then we need a loop to wait for events.
        // In this loop we will need to call the
        // pump() method to copy back events that have
        // already occured in the viewer thread inside
        // our thread.

        loop = true;
        while (loop) {
            fromViewer.pump();
        }
    }

    @Override
    public void viewClosed(String s) {
        loop = false;
    }

    @Override
    public void buttonPushed(String s) {
//        System.out.println("Button pushed on node " + s);
        if (graph.getNode(s).hasAttribute("ui.label")) {
            graph.getNode(s).removeAttribute("ui.label");
        } else {
            graph.getNode(s).addAttribute("ui.label", graph.getNode(s).getAttribute(Attribute.STATE).toString());
        }
    }

    @Override
    public void buttonReleased(String s) {
//        System.out.println("Button released on node " + s);
    }
}

