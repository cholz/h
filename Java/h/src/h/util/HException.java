package h.util;

//a class to represent exceptions encountered in the checking or interpretation of h programs
//not to be used for bugs e.g. null pointers or otherwise invalid preconditions
public class HException extends RuntimeException {

    public HException(String message) {
        super(message);
    }

}
