package h.model;

import h.checking.Match;
import h.checking.PatternFuns;
import h.pattern.*;
import h.pattern.List;
import h.parsing.Parse;

import java.util.*;

/**
 * Created by colinholzman on 8/28/17.
 */
public class Rule {

	public static final Rule IN_RULE = new Rule("(= () in (Any) )");

	public static final Rule OUT_RULE = new Rule("(= (v:Any) out () )");

    public static final Rule DROP_RULE = new Rule("(= (Any) drop () )");

    public static final Rule DUP_RULE = new Rule("(= (v:Any) dup (v v) )");

	public static final Rule SWAP_RULE = new Rule("(= (a:Any b:Any) swap (b a) )");

	public static final Rule PAIR_RULE = new Rule("(= (a:Any b:Any) pair ((a .b)) )");

    public static final Rule FST_RULE = new Rule("(= ((fst:Any .Any)) fst (fst) )");

    public static final Rule SND_RULE = new Rule("(= ((Any .snd:Any)) snd (snd) )");

    public static final Rule I_EMPTY_RULE = new Rule("(: (()) i )");

    public static final Rule I_SINGLE_RULE = new Rule("(: ((a:Any)) i a )");

    public static final Rule I_GENERAL_RULE = new Rule("(: ((f:Any .r:[List Any])) i f r i )");

    public static final Rule INC_RULE = new Rule("(= (i:Integer) inc (Integer) )");

    public static final Rule DEC_RULE = new Rule("(= (i:Integer) dec (Integer) )");

	public static final Rule LIT_INTEGER_RULE = new Rule("(= () n:Integer (n) )");

	public static final Rule LIT_TRUE_RULE = new Rule("(= () true (true) )");

	public static final Rule LIT_FALSE_RULE = new Rule("(= () false (false) )");

    public static final Rule LIT_PAIR_RULE = new Rule("(= () p:(Any .Any) (p) )");

    public static final Rule LIT_NIL_RULE = new Rule("(= () () (()) )");

    public static final Rule END_RULE = new Rule("(. ((end .Any) Any) -> ({} {}) )");

	private Pattern before;

	public Pattern getBefore() {
	    return before;
    }

	private Pattern after;

	public Pattern getAfter() {
	    return after;
    }

	Rule(String input) {
	    if (input == null) { throw new NullPointerException(); }
		Rule parsed = Parse.rule(input);
		this.before = parsed.before;
		this.after = parsed.after;
	}

	public Rule(Pattern before, Pattern after) {
        if (before == null || after == null) { throw new NullPointerException(); }
        this.before = before;
        this.after = after;
	}

	@Override
	public String toString() {
		return "(. " + getBefore().toString() + " -> " + getAfter().toString() + " )";
	}

	@Override
    public boolean equals(Object other) {
        return	other instanceof Rule
                && getBefore().equals(((Rule) other).getBefore())
                && getAfter().equals(((Rule) other).getAfter());
    }

	@Override
	public int hashCode() {
		return Arrays.hashCode(new int[] {getBefore().hashCode(), getAfter().hashCode()});
	}

	//return true if any of the before parts of the transitions is more specific than the corresponding in other
	public boolean isMoreSpecific(HContext ctx, Rule other) {
		if (other == null) { throw new NullPointerException(); }
		return PatternFuns.isMoreSpecific(ctx, getBefore().unwrapBindingPattern(), other.getBefore().unwrapBindingPattern());
	}

	//returns a new rule that matches anything that either lhs or rhs matches
	public static Rule union(HContext ctx, Rule lhs, Rule rhs) {
		if (lhs == null || rhs == null) { throw new NullPointerException(); }
		return new Rule(new Option(lhs.getBefore(), rhs.getBefore()).normalize(ctx), Nil.VALUE);
	}

	public static Rule union(HContext ctx, java.util.Set<Rule> rules) {
		if (rules == null) { throw new NullPointerException(); }
		if (rules.size() < 1) { throw new UnsupportedOperationException("union on zero rules"); }
		if (rules.size() == 1) {
			return rules.stream().findFirst().get();
		} else {
			Rule[] rulesArray = rules.toArray(new Rule[0]);
            Rule union = union(ctx, rulesArray[0], rulesArray[1]);
            for (int i = 2; i < rulesArray.length; ++i) {
            	union = union(ctx, union, rulesArray[i]);
			}
			return union;
		}
	}

	//orthogonal rules have orthogonal before parts
	public static boolean orthogonal(HContext ctx, Rule lhs, Rule rhs) {
	    if (lhs == null || rhs == null) { throw new NullPointerException(); }
	    return PatternFuns.orthogonal(ctx, lhs.getBefore().unwrapBindingPattern(), rhs.getBefore().unwrapBindingPattern());
    }

    //ambiguous rules are not orthogonal and each is more specific than the other
    public static boolean ambiguous(HContext ctx, Rule lhs, Rule rhs) {
        if (lhs == null || rhs == null) { throw new NullPointerException(); }
	    return !Rule.orthogonal(ctx, lhs, rhs) && lhs.isMoreSpecific(ctx, rhs) && rhs.isMoreSpecific(ctx, lhs);
    }

    public static int specificityComparer(HContext ctx, Rule lhs, Rule rhs) {
        if (lhs == null || rhs == null) { throw new NullPointerException(); }
        return lhs.isMoreSpecific(ctx, rhs) ? -1 : rhs.isMoreSpecific(ctx, lhs) ? 1 : 0;
    }

    public static boolean isRecursive(HContext ctx, Rule rule) {
	    if (rule == null) { throw new NullPointerException(); }
	    Pattern programPattern = State.getProgram(rule.getBefore());
	    if (programPattern instanceof Pair) {
	        Pattern namePattern = Pair.getFirst(programPattern);
            Pattern programTransform = State.getProgram(rule.getAfter());
            return List.contains(programTransform, (p) -> Match.possibleMatch(ctx, namePattern, p));
        } else {
	        return false;
        }
    }
}
