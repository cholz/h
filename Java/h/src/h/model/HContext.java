package h.model;

import h.parsing.Parse;
import h.pattern.Nil;
import h.pattern.Pattern;
import h.pattern.List;
import h.util.HException;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HContext {

    public static final HContext DEFAULT_CONTEXT = new HContext(
            Stream.of(
                Rule.IN_RULE         ,
                Rule.OUT_RULE        ,
                Rule.INC_RULE        ,
                Rule.DEC_RULE        ,
                Rule.DROP_RULE       ,
                Rule.DUP_RULE        ,
                Rule.SWAP_RULE       ,
                Rule.PAIR_RULE       ,
                Rule.FST_RULE        ,
                Rule.SND_RULE        ,
                Rule.I_EMPTY_RULE    ,
                Rule.I_SINGLE_RULE   ,
                Rule.I_GENERAL_RULE  ,
                Rule.LIT_INTEGER_RULE,
                Rule.LIT_FALSE_RULE  ,
                Rule.LIT_TRUE_RULE   ,
                Rule.LIT_PAIR_RULE   ,
                Rule.LIT_NIL_RULE    ,
                Rule.END_RULE
            ).collect(Collectors.toSet()),
            Stream.of(
                UserDefinedType.BOOLEAN_TYPE,
                UserDefinedType.LIST_TYPE
            ).collect(Collectors.toMap(t -> t.getName().getId(), t -> t)),
            List.create(Nil.VALUE, Nil.VALUE)
            );

    private java.util.Set<Rule> rules;

    public java.util.Set<Rule> getRules() {
        return new HashSet<>(rules);
    }

    private Map<String, UserDefinedType> types = new HashMap<>();

    public Map<String, UserDefinedType> getTypes() {
        return new HashMap<>(types);
    }

    private Pattern state;

    public Pattern getState() {
        return state;
    }

    public HContext(java.util.Set<Rule> rules, Map<String, UserDefinedType> types, Pattern state) {
        if (rules == null || types == null || state == null) { throw new NullPointerException(); }
        this.rules = rules;
        this.types = types;
        this.state = state;
    }

    public static HContext fromFileAugmentWithDefaults(String filename) throws HException {
        HContext ctx;
        //parse the context
        try {
            ctx = Parse.sp(new File(filename));
        } catch (ParseCancellationException e) {
            throw new HException(e.getLocalizedMessage());
        }

        //add default rules
        java.util.Set<Rule> rules = ctx.getRules();
        for (Rule rule : HContext.DEFAULT_CONTEXT.getRules()) {
            if (!rules.add(rule)) {
                throw new HException("rule " + rule.toString() + " already defined");
            }
        }

        //add default types
        Map<String, UserDefinedType> types = ctx.getTypes();
        for (String key : HContext.DEFAULT_CONTEXT.getTypes().keySet()) {
            if (types.put(key, HContext.DEFAULT_CONTEXT.getTypes().get(key)) != null) {
                throw new HException("type " + key + " already defined");
            }
        }

        //create new context with augmented rules and types
        return new HContext(rules, types, ctx.getState());
    }

}
