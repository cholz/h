package h.model;

import h.parsing.Parse;
import h.pattern.Pattern;
import h.pattern.Symbol;

import java.util.*;

public class UserDefinedType extends Pattern {

    public static final UserDefinedType BOOLEAN_TYPE = Parse.type("([Boolean] {true false})");
    public static final UserDefinedType LIST_TYPE = Parse.type("([List t] {() (t .[List t])})");

    private Symbol name;

    public Symbol getName() {
        return name;
    }

    private java.util.List<Symbol> formalParameters = new java.util.ArrayList<>();

    public java.util.List<Symbol> getFormalParameters() {
        return new ArrayList<>(formalParameters);
    }

    private Pattern pattern;

    public Pattern getPattern() {
        return pattern;
    }

    public UserDefinedType(Symbol name, java.util.List<Symbol> variables, Pattern pattern) {
        if (name == null || variables == null || pattern == null) { throw new NullPointerException(); }
        if (!name.isTypename()) { throw new IllegalArgumentException("name is not a type name"); }
        this.name = name;
        this.formalParameters = variables;
        this.pattern = pattern;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof UserDefinedType
                && getName().equals(((UserDefinedType) other).getName())
                && getFormalParameters().equals(((UserDefinedType) other).getFormalParameters())
                && getPattern().equals(((UserDefinedType) other).getPattern());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {getName(), getFormalParameters(), getPattern()});
    }

    @Override
    public String toString() {
        StringBuilder variablesString = new StringBuilder();
        getFormalParameters().forEach(v -> variablesString.append(v.toString() + " "));
        return "( " + "[ " + getName() + " " + variablesString.toString() + "] " + getPattern().toString() + " )";
    }

}
