package h.model;

import h.pattern.*;

import java.util.stream.Collectors;

public final class State {

    private State() {}

    public static Pattern create(Pattern program, Pattern stack) {
        if (program == null || stack == null) { throw new NullPointerException(); }
        return List.create(program, stack);
    }

    public static boolean isState(Pattern state) {
        if (state == null) { throw new NullPointerException(); }
        return List.isList(state) && List.length(state) == 2;
    }

    public static Pattern getProgram(Pattern state) {
        if (state == null) { throw new NullPointerException(); }
        if (!isState(state)) { throw new IllegalArgumentException("getProgram on something other than state"); }
        return Pair.getFirst(state);
    }

    public static Pattern getStack(Pattern state) {
        if (state == null) { throw new NullPointerException(); }
        if (!isState(state)) { throw new IllegalArgumentException("getStack on something other than state"); }
        return Pair.getFirst(Pair.getSecond(state));
    }

//    public static Pattern makeInitialState(Pattern state) {
//        if (state == null) { throw new NullPointerException(); }
//        if (!isState(state)) { throw new UnsupportedOperationException(); }
//        Pattern program = getProgram(state);
//        Pattern stack = getStack(state);
//        return State.create(simplifyForInitialState(program), simplifyForInitialState(stack));
//    }

//    private static Pattern simplifyForInitialState(Pattern pattern) {
//        if (pattern == null) { throw new NullPointerException(); }
//        if (pattern instanceof AnyType) {
//            return Nil.VALUE;
//        } else if (pattern instanceof Pair) {
//            return new Pair(simplifyForInitialState(Pair.getFirst(pattern)), simplifyForInitialState(Pair.getSecond(pattern)));
//        } else if (pattern instanceof Option) {
//            return Option.union(((Option) pattern).getElements().stream().map(e -> simplifyForInitialState(e)).collect(Collectors.toList()));
//        } else {
//            return pattern;
//        }
//    }

}
