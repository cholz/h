package h.checking;

//names of attributes to attach to graph nodes, edges
public enum Attribute {
    //nothing here
    ;

    //to attach a State to a node
    public static final String STATE = "state";

    //to attach a Rule to an edge
    public static final String RULE = "rule";

    //to indicate a starting node
    public static final String START = "start";

    //mark a node as visited
    public static final String VISITED = "visited";

}
