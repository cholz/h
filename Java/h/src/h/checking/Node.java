package h.checking;

import h.pattern.Pattern;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Node {

    private Node parent;

    public Node getParent() {
        return parent;
    }

    private Pattern state;

    public Pattern getState() {
        return state;
    }

    private java.util.Set<Edge> edges = new HashSet<Edge>();

    public java.util.Set<Edge> getEdges() {
        return new HashSet<>(edges);
    }

    public Node(Node parent, Pattern state, java.util.Set<Edge> edges) {
        if (state == null || edges == null) {
            throw new NullPointerException();
        }
        this.parent = parent;
        this.state = state;
        this.edges = edges;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof Node
                && (getParent() == ((Node) other).getParent() || (getParent() != null && getParent().equals(((Node) other).getParent())))
                && getState().equals(((Node) other).getState())
                && getEdges().equals(((Node) other).getEdges());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {getParent(), getState(), getEdges()});
    }

}
