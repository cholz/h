package h.checking;

import h.pattern.*;
import h.model.Rule;

import java.util.Map;
import java.util.stream.Collectors;

public final class Build {

    private Build() {}

    //substitute symbols found in pattern for the corresponding binding found in bindings.
    public static Pattern build(Pattern pattern, Map<String, Pattern> bindings) {
        if (pattern == null || bindings == null) { throw new NullPointerException(); }
        if (pattern instanceof Pair) {
            return new Pair(build(((Pair) pattern).getFirst(), bindings), build(((Pair) pattern).getSecond(), bindings));
        } else if (pattern instanceof Option) {
            return new Option(((Option) pattern).getElements().stream().map(e -> build(e, bindings)).collect(Collectors.toList()));
        } else if (pattern instanceof Symbol && bindings.containsKey(((Symbol) pattern).getId())) {
            return bindings.get(((Symbol) pattern).getId());
        } else {
            return pattern;
        }
    }

    //substitute symbols found in type constructor pattern for the corresponding binding found in bindings.
    public static Pattern buildTypeConstructor(Pattern pattern, Map<String, Pattern> bindings) {
        if (pattern == null || bindings == null) { throw new NullPointerException(); }
        if (pattern instanceof Pair) {
            return new Pair(buildTypeConstructor(((Pair) pattern).getFirst(), bindings), buildTypeConstructor(((Pair) pattern).getSecond(), bindings));
        } else if (pattern instanceof Option) {
            return new Option(((Option) pattern).getElements().stream().map(e -> buildTypeConstructor(e, bindings)).collect(Collectors.toList()));
        } else if (pattern instanceof Symbol && bindings.containsKey(((Symbol) pattern).getId())) {
            return bindings.get(((Symbol) pattern).getId());
        } else if (pattern instanceof TypeConstructor) {
            return new TypeConstructor(
                    ((TypeConstructor) pattern).getName(),
                    ((TypeConstructor) pattern).getActualParameters().stream().map(ap -> buildTypeConstructor(ap, bindings)).collect(Collectors.toList())
            );
        } else {
            return pattern;
        }
    }

    public static Pattern build(Rule rule, Map<String, Pattern> bindings) {
        if (rule == null || bindings == null) { throw new NullPointerException(); }
        return build(rule.getAfter(), bindings);
    }

}
