package h.checking;

import h.model.HContext;
import h.pattern.*;
import h.pattern.Integer;
import h.util.HException;
import h.util.StreamExtensions;
import scala.Int;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PatternFuns {

//    //find the actual number of values that a pattern represents
//    public static int countValues(HContext ctx, Pattern pattern) {
//        if (pattern.cardinality(ctx) == Pattern.Cardinality.INFINITE) {
//            //fail if pattern is infinite
//            throw new UnsupportedOperationException("pattern must not be infinite");
//        }
//        if (pattern instanceof Pair && pattern.cardinality(ctx) == Pattern.Cardinality.FINITE) {
//            //a pair pattern is like a product type, and as such its count of values
//            //is the product of the count of values of its first and second elements
//            return countValues(ctx, ((Pair) pattern).getFirst()) * countValues(ctx, ((Pair) pattern).getSecond());
//        } else if (pattern instanceof Option) {
//            //an option pattern is like a sum type, and as such its count of values
//            //is the sum of the count of values of all its elements
//            return ((Option) pattern).getElements().stream().map(e -> countValues(ctx, e)).reduce(0, (l, r) -> l + r);
//        } else if (pattern instanceof TypeConstructor) {
//            Pattern constructedPattern = ((TypeConstructor) pattern).construct(ctx);
//            return countValues(ctx, constructedPattern);
//        } else if (pattern instanceof NoneType) {
//            return 0;
//        } else {
//            //otherwise this is a literal value and its count is 1
//            return 1;
//        }
//    }

    //returns true if l is considered to be a more specific pattern than r, must unwrap binding patterns prior
    public static boolean isMoreSpecific(HContext ctx, Pattern lhs, Pattern rhs) throws HException {
        if (lhs == null || rhs == null) { throw new NullPointerException(); }

        if (lhs.equals(rhs)) {
            //if equal then not more specific
            return false;
        } else if (PatternFuns.isSubset(ctx, lhs, rhs)) {
            return true;
        } else if (lhs.cardinality(ctx).less(rhs.cardinality(ctx))) {
            //if lhs has fewer values then it is more specific
            return true;
        } else if (!lhs.isInfinite(ctx) && !rhs.isInfinite(ctx)) {
            //neither infinite so count values
            return lhs.countValues(ctx) < rhs.countValues(ctx);
        } else if (lhs instanceof Pair && rhs instanceof Pair) {
            //both pairs, lhs is more specific if either first or second
            //is more specific than the corresponding member in rhs
            return isMoreSpecific(ctx, ((Pair) lhs).getFirst(), ((Pair) rhs).getFirst())
                    || isMoreSpecific(ctx, ((Pair) lhs).getSecond(), ((Pair) rhs).getSecond());
        } else if (lhs instanceof Option && rhs instanceof Option) {
            if (((Option) lhs).getElements().size() == ((Option) rhs).getElements().size()) {
                return ((Option) lhs).getElements().stream().anyMatch(e -> isMoreSpecific(ctx, e, rhs));
            } else if (((Option) lhs).getElements().size() < ((Option) rhs).getElements().size()) {
                return true;
            } else {
                return false;
            }
        } else if (lhs instanceof TypeConstructor && rhs instanceof TypeConstructor
                && ((TypeConstructor) lhs).getName().equals(((TypeConstructor) rhs).getName())) {
            return StreamExtensions.zip(((TypeConstructor) lhs).getActualParameters().stream(), ((TypeConstructor) rhs).getActualParameters().stream())
                    .anyMatch(p -> isMoreSpecific(ctx, p.getKey(), p.getValue()));
        } else if (lhs instanceof TypeConstructor) {
            return isMoreSpecific(ctx, ((TypeConstructor) lhs).construct(ctx), rhs);
        } else if (rhs instanceof TypeConstructor) {
            return isMoreSpecific(ctx, lhs, ((TypeConstructor) rhs).construct(ctx));
        } else {
            //if we get here then we have something like
            //a 2
            //or
            //{1 2} {3 4}
            //and it can't be said either is more specific
            return false;
        }
    }

    //must unwrap binding patterns
    public static boolean isSubset(HContext ctx, Pattern lhs, Pattern rhs) throws HException {

        if (lhs == null || rhs == null) { throw new NullPointerException(); }

        if (lhs instanceof None || rhs instanceof None) {
            //{} is not a subset of anything
            //nothing is a subset of {}
            return false;
        } else if (rhs instanceof AnyType) {
            //everything is a subset of any
            return true;
        } else if (lhs.equals(rhs)) {
            //if equal then not a subset
            return false;
        } else if (lhs instanceof Pair && rhs instanceof Pair) {
            Pair left = (Pair)lhs;
            Pair right = (Pair)rhs;
            //two pairs, so either both left.getFirst and left.getSecond are subsets of the corresponding memebers of right
            //or one of getFirst and getSecond is a subset and the other is equal. Any of those and left is a subset of right
            return ((isSubset(ctx, left.getFirst(), right.getFirst()) && isSubset(ctx, left.getSecond(), right.getSecond()))
                    || (isSubset(ctx, left.getFirst(), right.getFirst()) && left.getSecond().equals(right.getSecond()))
                    || (left.getFirst().equals(right.getFirst()) && isSubset(ctx, left.getSecond(),right.getSecond())));
        } else if (lhs instanceof Option) {
            //look at all the elements of lhs they must all be a subset of rhs
            return ((Option) lhs).getElements().stream().allMatch(e -> e.equals(rhs) || isSubset(ctx, e, rhs));
        } else if (rhs instanceof Option) {
            //rhs is, lhs isn't an Option
            //lhs should equal or be a subset of at least one element of rhs
            return ((Option) rhs).getElements().stream().anyMatch(e -> lhs.equals(e) || isSubset(ctx, lhs, e));
        } else if (lhs instanceof TypeConstructor && rhs instanceof TypeConstructor
                && ((TypeConstructor) lhs).getName().equals(((TypeConstructor) rhs).getName())) {
            return StreamExtensions.zip(((TypeConstructor) lhs).getActualParameters().stream(), ((TypeConstructor) rhs).getActualParameters().stream())
                    .allMatch(p -> isSubset(ctx, p.getKey(), p.getValue()) || p.getKey().equals(p.getValue()));
        } else if (lhs instanceof TypeConstructor) {
            return isSubset(ctx, ((TypeConstructor) lhs).construct(ctx), rhs);
        } else if (rhs instanceof TypeConstructor) {
            return isSubset(ctx, lhs, ((TypeConstructor) rhs).construct(ctx));
        }  else if (rhs instanceof IntegerType) {
            //here we don't have to worry about lhs as Option because it's checked above just see if integer
            return lhs instanceof Integer;
        } else if (rhs instanceof SymbolType) {
            //like above
            return lhs instanceof Symbol;
        } else {
            //I think that's it
            return false;
        }
    }

    //return true if pat is a strict match for pattern e.g
    //a pattern of 5 does not match Integer because Integer may be 4 or 6 as well
    //but Integer matches 4, 5, 6, or any other Integer
    public static boolean strictMatch(HContext ctx, Pattern pattern, Pattern state) throws HException {

        if (pattern == null || state == null) { throw new NullPointerException(); }

        if (pattern instanceof None || state instanceof None) {
            //{} matches nothing
            //nothing matches {}
            return false;
        } else if (pattern instanceof AnyType) {
            //everything matches any
            return true;
        } else if (pattern.equals(state)) {
            //if equal then is a match
            return true;
        } else if (pattern instanceof Pair && state instanceof Pair) {
            //return true if both elements of pattern strictly match pattern
            return PatternFuns.strictMatch(ctx, ((Pair) pattern).getFirst(), ((Pair) state).getFirst())
                    && PatternFuns.strictMatch(ctx, ((Pair) pattern).getSecond(), ((Pair) state).getSecond());
        } else if (state instanceof Option) {
            //look at all the elements of state they must all strictly match the pattern
            return ((Option) state).getElements().stream().allMatch(e -> strictMatch(ctx, pattern, e));
        } else if (pattern instanceof Option) {
            //any element of pattern should strictly match the state
            return ((Option) pattern).getElements().stream().anyMatch(e -> strictMatch(ctx, e, state));
        } else if (pattern instanceof TypeConstructor && state instanceof TypeConstructor
                && ((TypeConstructor) pattern).getName().equals(((TypeConstructor) state).getName())) {
            //two type constructors with the same name, just look at actual parameters
            return StreamExtensions.zip(((TypeConstructor) pattern).getActualParameters().stream(), ((TypeConstructor) state).getActualParameters().stream())
                    .allMatch(p -> strictMatch(ctx, p.getKey(), p.getValue()));
        } else if (pattern instanceof TypeConstructor) {
            return strictMatch(ctx, ((TypeConstructor) pattern).construct(ctx), state);
        } else if (state instanceof TypeConstructor) {
            return strictMatch(ctx, pattern, ((TypeConstructor) state).construct(ctx));
        } else if (pattern instanceof IntegerType) {
            return state instanceof Integer || state instanceof IntegerType;
        } else if (pattern instanceof SymbolType) {
            return state instanceof Symbol || state instanceof SymbolType;
        } else {
            return false;
        }
    }

    //return true if pat is a possible match for obj
    //that means e.g. a pattern of 5 matches Integer
    //must unwrap binding patterns
    public static boolean possibleMatch(HContext ctx, Pattern pattern, Pattern state) {

        if (pattern == null || state == null) { throw new NullPointerException(); }

        if (pattern instanceof None
                || state instanceof None) {
            //{} matches nothing
            //nothing matches {}
            return false;
        } if (pattern instanceof AnyType
                || state instanceof AnyType) {
            //everything is a subset of any
            return true;
        } else if (pattern.equals(state)) {
            //if equal then not a subset
            return true;
        } else if (pattern instanceof Pair && state instanceof Pair) {
            return PatternFuns.possibleMatch(ctx, ((Pair) pattern).getFirst(), ((Pair) state).getFirst())
                    && PatternFuns.possibleMatch(ctx, ((Pair) pattern).getSecond(), ((Pair) state).getSecond());
        } else if (pattern instanceof Option) {
            //any element of pattern matching the state is a match
            return ((Option) pattern).getElements().stream().anyMatch(e -> possibleMatch(ctx, e, state));
        } else if (state instanceof Option) {
            //any element of state matching pattern is a match
            return ((Option) state).getElements().stream().anyMatch(e -> possibleMatch(ctx, pattern, e));
        } else if (pattern instanceof TypeConstructor && state instanceof TypeConstructor
                && ((TypeConstructor) pattern).getName().equals(((TypeConstructor) state).getName())) {
            //two type constructors with the same name, just look at actual parameters
            return StreamExtensions.zip(((TypeConstructor) pattern).getActualParameters().stream(), ((TypeConstructor) state).getActualParameters().stream())
                    .allMatch(p -> possibleMatch(ctx, p.getKey(), p.getValue()));
        } else if (pattern instanceof TypeConstructor) {
            return possibleMatch(ctx, ((TypeConstructor) pattern).construct(ctx), state);
        } else if (state instanceof TypeConstructor) {
            return possibleMatch(ctx, pattern, ((TypeConstructor) state).construct(ctx));
        } else if (pattern instanceof IntegerType) {
            return state instanceof Integer || state instanceof IntegerType;
        } else if (pattern instanceof SymbolType) {
            return state instanceof Symbol || state instanceof SymbolType;
        } else if (state instanceof IntegerType) {
            return pattern instanceof Integer;
        } else if (state instanceof SymbolType) {
            return pattern instanceof Symbol;
        } else {
            return false;
        }
    }

    //two patterns are orthogonal if they can never possibly match the same state
    //e.g. 2 and a are orthogonal because they will never match the same state, but
    //{2 b} {2 a} are not orthogonal because they share the matching state of 2
    //must unwrap binding patterns before use
    public static boolean orthogonal(HContext ctx, Pattern lhs, Pattern rhs) {
        if (lhs == null || rhs == null) { throw new NullPointerException(); }

        if (lhs instanceof None || rhs instanceof None) {
            //if either is none then they are orthogonal because neither will
            //ever match the same state
            return true;
        } else if (lhs instanceof AnyType || rhs instanceof AnyType) {
            //can't be orthogonal because one or both will match any state
            return false;
        } else if (lhs.isLiteral() && rhs.isLiteral()) {
            return !lhs.equals(rhs);
        } else if (lhs instanceof Pair && rhs instanceof Pair) {
            return orthogonal(ctx, ((Pair) lhs).getFirst(), ((Pair) rhs).getFirst())
                    || orthogonal(ctx, ((Pair) lhs).getSecond(), ((Pair) rhs).getSecond());
        } else if (lhs instanceof Option && rhs instanceof Option) {
            //for l in lhs, r in rhs:
            //  if l < r || r < l || l == r: return false
            return ((Option) lhs).getElements().stream().allMatch(lhsElem ->
                ((Option) rhs).getElements().stream().noneMatch(rhsElem ->
                    isSubset(ctx, lhsElem, rhsElem) || isSubset(ctx, rhsElem, lhsElem) || lhsElem.equals(rhsElem)
                )
            );
        } else if (lhs instanceof Option) {
            return ((Option) lhs).getElements().stream().allMatch(e -> orthogonal(ctx, e, rhs));
//            for (Pattern e : ((Option) lhs).getElements()) {
//                if (!orthogonal(ctx, e, rhs)) { return false; }
//            }
//            return true;
        } else if (rhs instanceof Option) {
            return ((Option) rhs).getElements().stream().allMatch(e -> orthogonal(ctx, lhs, e));
//            for (Pattern e : ((Option) rhs).getElements()) {
//                if (!orthogonal(ctx, lhs, e)) { return false; }
//            }
//            return true;
        } else if (lhs instanceof TypeConstructor && rhs instanceof TypeConstructor
            && ((TypeConstructor) lhs).getName().equals(((TypeConstructor) rhs).getName())) {
            if (((TypeConstructor) lhs).getActualParameters().isEmpty() && ((TypeConstructor) rhs).getActualParameters().isEmpty()) {
                //no parameters, and same name, not orthogonal
                return false;
            } else {
                //have parameters, construct each and ensure each construction is orthogonal
                return orthogonal(ctx, ((TypeConstructor) lhs).construct(ctx), ((TypeConstructor) rhs).construct(ctx));
            }
        } else if (lhs instanceof TypeConstructor) {
            return orthogonal(ctx, ((TypeConstructor) lhs).construct(ctx), rhs);
        } else if (rhs instanceof TypeConstructor) {
            return orthogonal(ctx, lhs, ((TypeConstructor) rhs).construct(ctx));
        } else if (lhs instanceof IntegerType) {
            return !(rhs instanceof IntegerType || rhs instanceof Integer);
        } else if (lhs instanceof SymbolType) {
            return !(rhs instanceof SymbolType || rhs instanceof Symbol);
        } else if (rhs instanceof IntegerType) {
            return !(lhs instanceof Integer);
        } else if (rhs instanceof SymbolType) {
            return !(lhs instanceof Symbol);
        } else {
            return !lhs.equals(rhs);
        }

    }

}
