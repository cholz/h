package h.checking;

import h.model.Rule;

import java.util.Arrays;

public class Edge {

    private Node target;

    public Node getTarget() {
        return target;
    }

    private Rule rule;

    public Rule getRule() {
        return rule;
    }

    //create an edge with target and rule
    public Edge(Node target, Rule rule) {
        if (target == null || rule == null) {
            throw new NullPointerException();
        }
        this.target = target;
        this.rule = rule;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof Edge
               && getTarget().equals(((Edge) other).getTarget())
               && getRule().equals(((Edge) other).getRule());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {getTarget(), getRule()});
    }

}
