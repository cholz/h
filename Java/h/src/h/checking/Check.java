package h.checking;

import h.util.GraphViewer;
import h.util.HException;
import h.util.IncompleteMatch;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import h.model.HContext;
import h.model.State;
import h.pattern.None;
import h.pattern.List;
import h.model.Rule;
import h.pattern.Pattern;

import java.util.*;
import java.util.stream.Collectors;

public class Check {

    static boolean isEmptyState(Pattern state) {
        return State.getProgram(state) instanceof None
            && State.getStack(state) instanceof None;
    }

    static boolean isStartNode(Node node) {
        return node.getAttribute(Attribute.START) != null;
    }

    //returns true if rules contains duplicate rules
//    static boolean containsDuplicateRules(HContext ctx, java.util.Set<Rule> rules) {
//        return rules.stream().anyMatch(r ->
//            rules.stream().anyMatch(l -> {
//                Pattern lBeforePatterns = l.getBefore();
//                Pattern rBeforePatterns = r.getBefore();
//                //if l == r then they are the same rule and we don't care, otherwise if their before parts are
//                //equal (while unwrapping binding patterns) then they are duplicate rules
//                if (!(l == r) && lBeforePatterns.unwrapBindingPattern().equals(rBeforePatterns.unwrapBindingPattern())) {
//                    System.out.println("duplicate rules: " + l.toString() + " and " + r.toString());
//                    return true;
//                } else {
//                    return false;
//                }
//            })
//        );
//    }

    //returns true if rules contains duplicate rules
    static boolean containsDuplicateRules(HContext ctx, java.util.Set<Rule> rules) {
        return rules.stream().anyMatch(r ->
                rules.stream().anyMatch(l -> {
                    Pattern lhs = l.getBefore().unwrapBindingPattern();
                    Pattern rhs = r.getBefore().unwrapBindingPattern();
                    //if l == r then they are the same rule and we don't care
                    //otherwise if their before parts are not orthogonal and neither is more specific than the other
                    //then return true we have found rules that are not unique
                    if (!(l == r)
                            && !PatternFuns.orthogonal(ctx, lhs, rhs)
                            && !(PatternFuns.isMoreSpecific(ctx, lhs, rhs) || PatternFuns.isMoreSpecific(ctx, rhs, lhs))) {
                        System.out.println("duplicate rules: " + l.toString() + " and " + r.toString());
                        return true;
                    } else {
                        return false;
                    }
                })
        );
    }

    static boolean containsAmbiguousRules(HContext ctx, java.util.Set<Rule> rules) {
        return rules.stream().anyMatch(l ->
            rules.stream().anyMatch(r -> {
                //all have to not be ambiguous, where ambiguous means each is more specific than the other
                if (Rule.ambiguous(ctx, l, r)) {
                    System.out.println("ambiguous rules: " + l.toString() + " and " + r.toString());
                    return true;
                } else {
                    return false;
                }
            })
        );
    }

    static boolean isCompleteMatch(HContext ctx, Pattern state, java.util.Set<Rule> matches) throws HException {
        if (matches.size() > 0) {
            Rule unionRule = Rule.union(ctx, matches);
            Pattern expandedState = state.expand(ctx).normalize(ctx);
            if (!Match.strictMatch(ctx, unionRule, expandedState)) {
                System.out.println("incomplete match " + state.toString());
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    static boolean checkWellTerminated(Graph graph, String headId) {
        Node node = graph.getNode(headId);
        Pattern state = node.getAttribute(Attribute.STATE);
        if (node.getOutDegree() == 0) {
            return isEmptyState(state);
        } else {
            node.addAttribute(Attribute.VISITED, true);
            return node.getLeavingEdgeSet().stream().allMatch(e -> {
                Node targetNode = e.getTargetNode();
                if (targetNode.getAttribute(Attribute.VISITED) != null) {
                    return true;
                } else {
                    return checkWellTerminated(graph, e.getTargetNode().getId());
                }
            });
        }
    }

    //find a node with the same set of matches as abstractState
    static String findEquivalentNode(HContext ctx, Graph graph, String fromNodeId, Pattern state, java.util.Set<Rule> rules) {
        Node fromNode = graph.getNode(fromNodeId);
        //starting with fromNode, go backward until we find a
        java.util.Set<Rule> matchSet = Match.possibleMatch(ctx, rules, state);
        if (!matchSet.stream().anyMatch(r -> Rule.isRecursive(ctx, r))) {
            return null;
        }
        for (Node n: graph.getNodeSet()) {
            Pattern existingState = n.getAttribute(Attribute.STATE);
            java.util.Set<Rule> existingMatchSet = Match.possibleMatch(ctx, rules, existingState);
            if (existingMatchSet.equals(matchSet)) {
                //found a node with the same match set
                if (List.length(State.getProgram(state)) == List.length(State.getProgram(existingState))
                        && List.length(State.getStack(state)) == List.length(State.getStack(existingState))) {
                    //if there are the same number of things on the stack and program then return found node
                    return n.getId();
                } else {
                    System.out.println("possible ill formed program");
                    //TODO: report errors
//                        throw new UnsupportedOperationException();
                }
            }
        }
        return null;
    }

    //id for new nodes in the graph
    private static long id = 0;

    //when a rule has matched on the state at nodeId then we
    private static void addMatch(HContext ctx, Graph graph, String fromNodeId, java.util.Set<Rule> rules, Rule matchedRule) throws HException {

        Pattern state = graph.getNode(fromNodeId).getAttribute(Attribute.STATE);

        //possibleMatchBind variables
        Map<String, Pattern> bindings = Bind.possibleMatchBind(ctx, matchedRule, state);

        //build set of new states, exapanded so binding works
        Pattern newState = Build.build(matchedRule, bindings);//.expand(ctx);

//        java.util.Set<Pattern> newStates = newBuiltState.toSet(ctx);
//
//        for (Pattern newState : newStates) {

            //new to node id (or the id of newState if it's already in the graph)
            String newNodeId = String.valueOf(id++);
            String equivalentNode = null;
            equivalentNode = findEquivalentNode(ctx, graph, fromNodeId, newState, rules);
            if (equivalentNode != null) {
                //don't add existing if the matched rule is not recursive and we didn't find an equivalent node
                newNodeId = equivalentNode;
            } else {
                //add new id
                graph.addNode(newNodeId);
                graph.getNode(newNodeId).addAttribute(Attribute.STATE, newState);
            }

            //new edge id
            String edgeId = String.valueOf(id++);

            //add the edge
            graph.addEdge(edgeId, fromNodeId, newNodeId, true);

            //attach edge attributes
            graph.getEdge(edgeId).addAttribute(Attribute.RULE, matchedRule);

            //if it was a new state then recurse on that state
            if (equivalentNode == null) {
                //recurse in the case that there wasn't an existing node
                buildGraphFromNode(ctx, graph, newNodeId, rules);
            }

//        }

    }

    private static void buildGraphFromNode(HContext ctx, Graph graph, String nodeId, java.util.Set<Rule> rules) throws HException {

        Node node = graph.getNode(nodeId);

        Pattern state = node.getAttribute(Attribute.STATE);

        if (isEmptyState(state) && !isStartNode(node)) {
            //if the node is empty and it's not the start node, then don't try to strictMatch a rule
            //because we'll just end up matching the start rule
            return;
        }

        //sort by specificity, with most specific first
        java.util.List<Rule> matched = Match.possibleMatch(ctx, rules, state).stream().sorted((l, r) -> Rule.specificityComparer(ctx, l, r)).collect(Collectors.toList());

        //System.out.print(state.toString() + " matched rules: ");
        //matched.forEach(r -> System.out.print(r.toString() + ", "));
        //System.out.println();

        //keep a set of rules we have already built
        java.util.Set<Rule> built = new HashSet<>();

        //for each of the matched rules most specific first
        for (Rule rule: matched) {

            //add a match node
            addMatch(ctx, graph, nodeId, rules, rule);

            built.add(rule);

            if (isCompleteMatch(ctx, state, built)) {
                break;
            }

        }

        //TODO: report errors
        if (!isCompleteMatch(ctx, state, built)) {
            throw new IncompleteMatch("incomplete match on state " + state);
        }

    }

    //builds graph and returns id of head
    private static String buildGraph(HContext ctx, Graph graph, Pattern initialState, java.util.Set<Rule> rules) throws HException {

        //get a new id
        String stateId = String.valueOf(id++);

        //add the node
        graph.addNode(stateId).addAttribute(Attribute.STATE, initialState);

        graph.getNode(stateId).addAttribute(Attribute.START, true);

        //build from that node
        buildGraphFromNode(ctx, graph, stateId, rules);

        return stateId;

    }

    //find nodes that have inDegree == outDegree == 1 and get rid of them
    private static void compressGraph(Graph graph) {
        if (graph == null) { throw new NullPointerException(); }
        java.util.Set<Node> nodes = new HashSet<Node>(graph.getNodeSet());
        while (nodes.size() > 0) {
            Node nodeA = nodes.iterator().next();
            //--n-> A --m->
            if (   nodeA.getInDegree() == 1
                && nodeA.getOutDegree() == 1) {
                //graph looks like:
                //B --0-> A --1-> C
                //make it look like
                //B --new-> C
                Edge edge0 = nodeA.getEnteringEdge(0);
                Node nodeB = edge0.getSourceNode();
                Edge edge1 = nodeA.getLeavingEdge(0);
                Node nodeC = edge1.getTargetNode();
                String newEdgeId = String.valueOf(id++);
                Edge newEdge = graph.addEdge(newEdgeId, nodeB, nodeC, true);
                newEdge.addAttribute(Attribute.RULE, edge0.getAttribute(Attribute.RULE) + " -> " + nodeA.getAttribute(Attribute.STATE) + " -> " + edge1.getAttribute(Attribute.RULE));
                graph.removeEdge(edge0);
                if (edge0 != edge1) graph.removeEdge(edge1);
                graph.removeNode(nodeA);
            }
            nodes.remove(nodeA);
        }
    }

    private static void labelGraph(Graph graph) {
        for (Node n : graph.getNodeSet()) {
            if (n.getAttribute(Attribute.START) != null) {
                n.addAttribute("ui.style", "fill-color: rgb(0,255,0);");
            }
            n.removeAttribute("ui.label");
            n.removeAttribute("layout.weight");
            for (Edge e : n.getLeavingEdgeSet()) {
                e.removeAttribute("ui.label");
                e.removeAttribute("layout.weight");
            }
//            if (n.getOutDegree() > 1 || n.getInDegree() > 1) {
                n.addAttribute("ui.label", n.getAttribute(Attribute.STATE).toString());
                n.addAttribute("layout.weight", 5);
                for (Edge e : n.getLeavingEdgeSet()) {
                    e.addAttribute("ui.label", e.getAttribute(Attribute.RULE).toString());
                    e.addAttribute("layout.weight", 5);
                }
//            } else if (n.getOutDegree() == 0) {
//                n.addAttribute("ui.label", n.getAttribute(Attribute.STATE).toString());
//                n.addAttribute("layout.weight", 5);
//                for (Edge e : n.getEnteringEdgeSet()) {
//                    e.addAttribute("ui.label", e.getAttribute(Attribute.RULE).toString());
//                    e.addAttribute("layout.weight", 5);
//                }
//            } else if (n.getInDegree() == 0) {
//                n.addAttribute("ui.label", n.getAttribute(Attribute.STATE).toString());
//                n.addAttribute("layout.weight", 5);
//                for (Edge e : n.getLeavingEdgeSet()) {
//                    e.addAttribute("ui.label", e.getAttribute(Attribute.RULE).toString());
//                    e.addAttribute("layout.weight", 5);
//                }
//            }
        }
    }

    public static boolean check(HContext ctx) throws HException {

        Pattern state = ctx.getState();
        java.util.Set<Rule> rules = ctx.getRules();

        //handle duplicate rules
        if (containsDuplicateRules(ctx, rules)) {
            System.out.println("duplicate rules");
            return false;
        }

        //and ambiguous rules here
        if (containsAmbiguousRules(ctx, rules)) {
            System.out.println("ambiguous rules");
            return false;
        }

        Graph graph = new MultiGraph("");
        graph.setStrict(false);
        graph.setAutoCreate(true);

        String startId;
        try {
            startId = buildGraph(ctx, graph, state, rules);
        } catch (IncompleteMatch e) {
            System.out.println(e.getMessage());
            return false;
        }

        if (!checkWellTerminated(graph, startId)) {
            System.out.println("not well terminated");
            return false;
        }

        new GraphViewer(graph).view();

        return true;
    }

    public static boolean checkFile(String filename) {

        HContext ctx;
        try {
            ctx = HContext.fromFileAugmentWithDefaults(filename);
        } catch (ParseCancellationException | HException e) {
            //TODO: better message
            e.printStackTrace();
            return false;
        }

        //check context
        try {
            if (!Check.check(ctx)) {
                System.out.println("checking failed");
                return false;
            }
        } catch (HException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
