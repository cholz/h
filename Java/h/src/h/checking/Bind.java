package h.checking;

import h.model.HContext;
import h.parsing.Parse;
import h.pattern.*;
import h.model.Rule;
import h.pattern.Integer;
import h.util.HException;
import h.util.StreamExtensions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static h.checking.PatternFuns.isSubset;
import static h.checking.PatternFuns.possibleMatch;

public class Bind {

    private static void addBinding(Map<String, Pattern> bindings, String name, Pattern state) throws HException {
        if (bindings == null || state == null) { throw new NullPointerException(); }
        if (name != null) {
            if (bindings.put(name, state) != null) {
                throw new HException(name + " previously bound");
            }
        }
    }

    private static void nop() {}

    //attempt to match pattern to state and make add correct bindings if possible
    //state must not contain binding patterns
    //returns matched state, or null if no match
    public static Pattern possibleMatchBind(HContext ctx, Map<String, Pattern> bindings, Pattern pattern, Pattern state) throws HException {

        if (bindings == null || pattern == null || state == null) { throw new NullPointerException(); }

        String name = null;

        if (pattern instanceof BindingPattern) {
            name = ((BindingPattern) pattern).getName().getId();
            pattern = ((BindingPattern) pattern).getPattern();
        }

        if (pattern instanceof None || state instanceof None) {
            //{} matches nothing
            //nothing matches {}
            return null;
        } if (pattern instanceof AnyType) {
            addBinding(bindings, name, state);
            return state;
        } else if (pattern.equals(state)) {
            addBinding(bindings, name, state);
            return state;
        } else if (pattern instanceof Pair && state instanceof Pair) {
            if (possibleMatchBind(ctx, bindings, ((Pair) pattern).getFirst(), ((Pair) state).getFirst()) != null
                    && possibleMatchBind(ctx, bindings, ((Pair) pattern).getSecond(), ((Pair) state).getSecond()) != null) {
                if (isSubset(ctx, pattern, state)) {
                    state = pattern.unwrapBindingPattern();
                }
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (pattern instanceof Pair && state instanceof AnyType) {
            //this condition is to bind the inner elements of a pair pattern to themselves
            //e.g. the pattern (a:(1.2)) should bind a:(1.2) against the state Any
            if (possibleMatchBind(ctx, bindings, ((Pair) pattern).getFirst(), state) != null
                    && possibleMatchBind(ctx, bindings, ((Pair) pattern).getSecond(), state) != null) {
                addBinding(bindings, name, pattern.unwrapBindingPattern());
                return state;
            } else {
                return null;
            }
        } else if (pattern instanceof Option) {
            //any element of pattern matching the state is a match
            for (Pattern element : ((Option) pattern).getElements()) {
                //call possibleMatch here because we're not binding things found inside Options
                if (possibleMatchBind(ctx, bindings, element.unwrapBindingPattern(), state) != null) {
                    if (isSubset(ctx, pattern, state)) {
                        state = pattern.unwrapBindingPattern();
                    }
                    addBinding(bindings, name, state);
                    return state;
                }
            }
            return null;
        } else if (state instanceof Option) {
            //any element of state matching pattern is a match
            //add every element of state that is a match to matched elements
            Set<Pattern> matchedElements = new HashSet<>();
            for (Pattern element : ((Option) state).getElements()) {
                if (possibleMatchBind(ctx, bindings, pattern, element) != null) {
                    matchedElements.add(element);
                }
            }
            //if some elements were matched then possibleMatchBind this pattern
            //to the option formed from the matched elements
            if (matchedElements.size() != 0) {
                state = new Option(matchedElements).normalize(ctx);
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (pattern instanceof TypeConstructor && state instanceof TypeConstructor
                && ((TypeConstructor) pattern).getName().equals(((TypeConstructor) state).getName())) {
            //two type constructors with the same name, just look at actual parameters
            if (StreamExtensions.zip(((TypeConstructor) pattern).getActualParameters().stream(), ((TypeConstructor) state).getActualParameters().stream())
                    .allMatch(p -> possibleMatch(ctx, p.getKey(), p.getValue()))) {
                if (isSubset(ctx, pattern, state)) {
                    state = pattern.unwrapBindingPattern();
                }
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (pattern instanceof TypeConstructor) {
            if (possibleMatch(ctx, ((TypeConstructor) pattern).construct(ctx), state)) {
                if (isSubset(ctx, pattern, state)) {
                    state = pattern.unwrapBindingPattern();
                }
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (state instanceof TypeConstructor) {
            Pattern match = possibleMatchBind(ctx, bindings, pattern, ((TypeConstructor) state).construct(ctx));
            if (match != null) {
                state = match;
                if (isSubset(ctx, pattern, state)) {
                    state = pattern.unwrapBindingPattern();
                }
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (pattern instanceof IntegerType) {
            if (state instanceof Integer || state instanceof IntegerType) {
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (pattern instanceof SymbolType) {
            if (state instanceof Symbol || state instanceof SymbolType) {
                addBinding(bindings, name, state);
                return state;
            } else {
                return null;
            }
        } else if (state instanceof IntegerType) {
            if (pattern instanceof Integer) {
                addBinding(bindings, name, pattern);
                return pattern;
            } else {
                return null;
            }
        } else if (state instanceof SymbolType) {
            if (pattern instanceof Symbol) {
                addBinding(bindings, name, pattern);
                return pattern;
            } else {
                return null;
            }
        } else if (state instanceof AnyType) {
            addBinding(bindings, name, pattern.unwrapBindingPattern());
            return pattern.unwrapBindingPattern();
        } else {
            return null;
        }
    }

    static Map<String, Pattern> bind(HContext ctx, Pattern pattern, Pattern object) throws HException {
        if (pattern == null || object == null) {
            throw new NullPointerException();
        }

        Map<String, Pattern> ret = new HashMap<>();

        object = object.unwrapBindingPattern();

        if (pattern instanceof BindingPattern) {
            //if the pattern is a binding pattern then possibleMatchBind the name
            //of the binding pattern to the object it matched

            //get the name to possibleMatchBind to
            Symbol name = ((BindingPattern) pattern).getName();
            //unwrap the actual pattern
            pattern = ((BindingPattern) pattern).getPattern();
            //check if the object to possibleMatchBind is a type
            if (PatternFuns.isSubset(ctx, pattern, object)) {
                //if the pattern is a subset of the object but it matched through abstract match
                //then possibleMatchBind the pattern not the object to have a more complete picture of the execution
                object = pattern.unwrapBindingPattern();
            }
            //possibleMatchBind name to object and check for duplicate bindings
            if (ret.put(name.getId(), object) != null) {
                throw new HException(name + " previously bound");
            }

        } else if (pattern instanceof Pair) {
//            //if the pattern is a pair then recurse
            if (!(object instanceof Pair)) {
                //object might not be a Pair when the pattern is a Pair
                //e.g. if object is Any then Pair will possibly match
                //or if object is [List Any] then Pair will possibly match
                //if object is [List Any] and pattern is Pair, then object should be set to (Any .[List Any])
                //TODO: a better way to deal with this
                object = pattern;
            }
            //possibleMatchBind both halves of a pair
            ret.putAll(bind(ctx, ((Pair) pattern).getFirst(), ((Pair)object).getFirst()));
            ret.putAll(bind(ctx, ((Pair) pattern).getSecond(), ((Pair)object).getSecond()));
        }
        return ret;
    }

    public static Map<String, Pattern> bind(HContext ctx, Rule rule, Pattern state) throws HException {
        if (rule == null || state == null) {
            throw new NullPointerException();
        }
        if (!Match.possibleMatch(ctx, rule, state)) {
            throw new HException("can't bind a pattern that doesn't match");
        }
        Map<String, Pattern> ret = new HashMap<>();
        ret.putAll(bind(ctx, rule.getBefore(), state));
        return ret;
    }

    public static Map<String, Pattern> possibleMatchBind(HContext ctx, Rule rule, Pattern state) throws HException {
        if (rule == null || state == null) {
            throw new NullPointerException();
        }
        if (!Match.possibleMatch(ctx, rule, state)) {
            throw new HException("can't bind a pattern that doesn't match");
        }
        Map<String, Pattern> ret = new HashMap<>();
        possibleMatchBind(ctx, ret, rule.getBefore(), state);
        return ret;
    }

}
