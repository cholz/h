package h.checking;

import h.model.HContext;
import h.pattern.Pattern;
import h.model.Rule;

import java.util.Map;
import java.util.stream.Collectors;

public final class Match {

    private Match() {}

    //returns a create of all matches found where a strictMatch is either when
    // pattern is a subset of pattern or is equal to pattern
    public static boolean strictMatch(HContext ctx, Pattern pattern, Pattern object) {
        if (pattern == null || object == null) { throw new NullPointerException(); }
        return PatternFuns.strictMatch(ctx, pattern.unwrapBindingPattern(), object.unwrapBindingPattern());
    }

    //
    public static boolean possibleMatch(HContext ctx, Pattern pattern, Pattern object) {
        if (pattern == null || object == null) { throw new NullPointerException(); }
        return PatternFuns.possibleMatch(ctx, pattern.unwrapBindingPattern(), object.unwrapBindingPattern());
    }

    //return true if rule matches state using strictMatch
    public static boolean strictMatch(HContext ctx, Rule rule, Pattern state) {
        if (rule == null || state == null) { throw new NullPointerException(); }
        return Match.strictMatch(ctx, rule.getBefore(), state);
    }

    //return true if rule matches state using possibleMatch
    public static boolean possibleMatch(HContext ctx, Rule rule, Pattern state) {
        if (rule == null || state == null) { throw new NullPointerException(); }
        return Match.possibleMatch(ctx, rule.getBefore(), state);
    }

    //returns the rules that strictly match state
    public static java.util.Set<Rule> strictMatch(HContext ctx, java.util.Set<Rule> rules, Pattern state) {
        if (rules == null || state == null) { throw new NullPointerException(); }
        return rules.stream().filter(r -> strictMatch(ctx, r, state)).collect(Collectors.toSet());
    }

    //returns the rules that abstractly match state
    public static java.util.Set<Rule> possibleMatch(HContext ctx, java.util.Set<Rule> rules, Pattern state) {
        if (rules == null || state == null) { throw new NullPointerException(); }
        return rules.stream().filter(r -> possibleMatch(ctx, r, state)).collect(Collectors.toSet());
    }

}
