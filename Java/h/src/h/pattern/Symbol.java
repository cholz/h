package h.pattern;

import h.model.HContext;

import java.util.*;

/**
 * Created by colinholzman on 8/28/17.
 */
public class Symbol extends Pattern {

    private static int gensym = 0;

    private static java.util.Set<String> ids = new HashSet<>();

    private String id;

    public String getId() {
        return id;
    }

    public Symbol() {
        String id;
        do {
            id = "$" + String.valueOf(gensym++);
        } while(ids.contains(id));
        ids.add(id);
        this.id = id;
    }

    public Symbol(String id) {
        if (id == null) { throw new NullPointerException(); }
        ids.add(id);
        this.id = id;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof Symbol
                && getId().equals(((Symbol) other).getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return getId();
    }

    public boolean isTypename() {
        return Character.isUpperCase(getId().charAt(0));
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public int countValues(HContext ctx) {
        return 1;
    }

}
