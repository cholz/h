package h.pattern;

import h.checking.PatternFuns;
import h.model.HContext;

import java.util.*;
import java.util.Set;
import java.util.stream.Collectors;

public class Option extends Pattern {

    private java.util.Set<Pattern> elements = new HashSet<>();

    public Set<Pattern> getElements() {
        return new HashSet<>(elements);
    }

    public Option() {}

    public Option(Pattern... elements) {
        this.elements.addAll(Arrays.asList(elements));
    }

    public Option(java.util.List<Pattern> elements) {
        if (elements == null) { throw new NullPointerException(); }
        this.elements.addAll(elements);
    }

    public Option(java.util.Set<Pattern> elements) {
        if (elements == null) { throw new NullPointerException(); }
        this.elements.addAll(elements);
    }

	@Override
	public boolean equals(Object other) {
		return other instanceof Option
                && getElements().equals(((Option) other).getElements());
	}

	@Override
	public Pattern unwrapBindingPattern() {
        return new Option(getElements().stream().map(e -> e.unwrapBindingPattern()).collect(Collectors.toList()));
    }

	@Override
	public int hashCode() {
		return getElements().hashCode();
	}

	@Override
	public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        getElements().forEach(e -> sb.append(e.toString()).append(" "));
        sb.append("}");
        return sb.toString();
	}

	@Override
    public Pattern expand(HContext ctx) {
        return new Option(getElements().stream().map(e -> e.expand(ctx)).collect(Collectors.toList()));
    }

    @Override
    public java.util.Set<Pattern> toSet(HContext ctx) {
        return getElements();
    }

    @Override
    public boolean contains(Pattern p) {
	    return getElements().stream().anyMatch(e -> e.contains(p));
    }

    @Override
    public boolean isInfinite(HContext ctx) {
        return getElements().stream().anyMatch(e -> e.isInfinite(ctx));
    }

    @Override
    public int countValues(HContext ctx) {
        return getElements().stream().map(e -> e.countValues(ctx)).reduce(0, (l, r) -> l + r);
    }

    @Override
    public Pattern flatten(HContext ctx) {
        Set<Pattern> result = new HashSet<>();
        getElements().forEach(e -> {
            if (e instanceof Option) {
                result.addAll(((Option) e).getElements());
            } else if (e instanceof None) {
                //don't add None
            } else {
                result.add(e);
            }
        });
        return new Option(result);
    }

    @Override
    public Pattern normalize(HContext ctx) {
        //get a flattened version of elements
        Set<Pattern> elements = ((Option)flatten(ctx)).getElements().stream().map(e -> e.unwrapBindingPattern().normalize(ctx)).collect(Collectors.toSet());
        //a set to contain the elements to remove
        Set<Pattern> toRemove = new HashSet<>();
        //loop over elements once
        elements.forEach(lhs -> {
            //and again
            elements.forEach(rhs -> {
                //if an element is a subset of another then add it to the set of elements to remove
                if (lhs != rhs) {
                    if (PatternFuns.isSubset(ctx, lhs, rhs)) {
                        toRemove.add(lhs);
                    }
                    if (PatternFuns.isSubset(ctx, rhs, lhs)) {
                        toRemove.add(rhs);
                    }
                }
            });
        });
        //remove the elements that were found to be subsets of another element
        elements.removeAll(toRemove);
        //we're left with the normalized set
        if (elements.size() == 0) {
            //if it's empty then return {}
            return None.VALUE;
        } else if (elements.size() == 1) {
            //just one element return that element
            return elements.stream().findFirst().get();
        } else {
            //else return an option of those elements
            return new Option(elements);
        }
    }

//    public static Pattern union(HContext ctx, Pattern l, Pattern r) {
//        if (l == null || r == null) { throw new NullPointerException(); }
//
//        Pattern lhs = l.unwrapBindingPattern();
//        Pattern rhs = r.unwrapBindingPattern();
//        Set<Pattern> elements = new HashSet<>();
//
//        if (lhs instanceof None && rhs instanceof None) {
//            //no elements
//        } else if (lhs instanceof None) {
//            elements.add(rhs);
//        } else if (rhs instanceof None) {
//            elements.add(lhs);
//        } else if (PatternFuns.isSubset(ctx, lhs, rhs)) {
//            elements.add(rhs);
//        } else if (PatternFuns.isSubset(ctx, rhs, lhs)) {
//            elements.add(lhs);
//        } else if (lhs instanceof Option && rhs instanceof Option) {
//            elements.addAll(((Option) lhs).getElements());
//            elements.addAll(((Option) rhs).getElements());
//        } else if (lhs instanceof Option) {
//            elements.addAll(((Option) lhs).getElements());
//            elements.add(rhs);
//        } else if (rhs instanceof Option) {
//            elements.addAll(((Option) rhs).getElements());
//            elements.add(lhs);
//        } else {
//            elements.add(lhs);
//            elements.add(rhs);
//        }
//        return new Option(elements).normalize(ctx);
//    }

}
