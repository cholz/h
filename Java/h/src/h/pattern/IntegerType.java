package h.pattern;

import h.model.HContext;

public class IntegerType extends Pattern {

    public static final IntegerType VALUE = new IntegerType();

    private IntegerType() {}

    @Override
    public boolean equals(Object other) {
        return other instanceof IntegerType;
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public String toString() {
        return "Integer";
    }

    @Override
    public boolean isInfinite(HContext ctx) {
        return true;
    }

    @Override
    public int countValues(HContext ctx) {
        throw new UnsupportedOperationException("can't count the values of Integer");
    }

}
