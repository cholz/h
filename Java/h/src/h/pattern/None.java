package h.pattern;

import h.model.HContext;

public class None extends Pattern {

    public static final None VALUE = new None();

    private None() {}

    @Override
    public boolean equals(Object other) {
        return other instanceof None;
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public String toString() {
        return "{}";
    }

    @Override
    public int countValues(HContext ctx) {
        return 0;
    }

}
