package h.pattern;

import h.model.HContext;

/**
 * Created by colinholzman on 8/29/17.
 */
public class Nil extends Pattern {

    public static final Nil VALUE = new Nil();

    private Nil() {}

    @Override
    public boolean equals(Object other) {
        return other instanceof Nil;
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public String toString() {
        return "()";
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public int countValues(HContext ctx) {
        return 1;
    }

}
