package h.pattern;

import com.sun.xml.internal.ws.handler.HandlerException;
import h.checking.Build;
import h.model.HContext;
import h.model.UserDefinedType;
import h.util.HException;

import java.util.*;
import java.util.List;

public class TypeConstructor extends Pattern {

    private Symbol name;

    public Symbol getName() {
        return name;
    }

    private java.util.List<Pattern> actualParameters = new ArrayList<>();

    public List<Pattern> getActualParameters() {
        return new ArrayList<>(actualParameters);
    }

    public TypeConstructor(Symbol name, java.util.List<Pattern> arguments) {
        if (name == null || arguments == null) { throw new NullPointerException(); }
        if (!name.isTypename()) { throw new UnsupportedOperationException("name must be a type name"); }
        this.name = name;
        this.actualParameters = arguments;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof TypeConstructor
                && getName().equals(((TypeConstructor) other).getName())
                && getActualParameters().equals(((TypeConstructor) other).getActualParameters());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {getName(), getActualParameters()});
    }

    @Override
    public String toString() {
        StringBuilder argumentsString = new StringBuilder();
        getActualParameters().forEach(v -> argumentsString.append(v.toString() + " "));
        return "[ " + getName() + " " + argumentsString.toString() + "]";
    }

    @Override
    public Pattern expand(HContext ctx) throws HException {
        Pattern constructed = construct(ctx);
        if (constructed instanceof Option) {
            //if this type constructor makes an option, return that option so that callers can expand properly
            return constructed;
        } else {
            //don't expand
            //TODO: this probably isn't right in the case that constructed is a pair
            return this;
        }
    }

    //returns true if this type constructor is recursive in the given context
    public boolean isRecursive(HContext ctx) throws HException {
        Map<String, UserDefinedType> typeMap = ctx.getTypes();
        if (!typeMap.containsKey(getName().getId())) {
            throw new HException("type " + getName().getId() + " not defined");
        }
        return construct(ctx).contains(this);
    }

    public Pattern construct(HContext ctx) throws HException {
        Map<String, UserDefinedType> typeMap = ctx.getTypes();
        if (!typeMap.containsKey(getName().getId())) {
            throw new HException("type " + getName().getId() + " not defined");
        }
        UserDefinedType typeDef = typeMap.get(getName().getId());
        java.util.List<Symbol> variables = typeDef.getFormalParameters();
        Pattern pattern = typeDef.getPattern();
        Map<String, Pattern> actualParameterBindings = new HashMap<>();
        //ensure correct number of actual actualParameters
        if (getActualParameters().size() != variables.size()) {
            throw new HException("incorrect number of actual parameters to " + toString());
        }
        for (int i = 0; i < variables.size(); ++i) {
            actualParameterBindings.put(variables.get(i).getId(), getActualParameters().get(i));
        }
        return Build.buildTypeConstructor(pattern, actualParameterBindings);
    }

    @Override
    public boolean isInfinite(HContext ctx) throws HException {
        return isRecursive(ctx) || construct(ctx).isInfinite(ctx);
    }

    @Override
    public int countValues(HContext ctx) throws HException {
        if (isInfinite(ctx)) {throw new UnsupportedOperationException("can't count infinite values");}
        return construct(ctx).countValues(ctx);
    }

}
