package h.pattern;

import java.util.function.Function;

public final class List {

    private List() {}

    public static Pattern create(Pattern...e) {
        Pattern ret = Nil.VALUE;
        for (int i = e.length; i > 0; --i) {
            ret = new Pair(e[i - 1], ret);
        }
        return ret;
    }

    public static boolean isList(Pattern pair) {
        if (pair == null) { throw new NullPointerException(); }
        return pair instanceof Nil
                || pair instanceof Pair
                && isList(Pair.getSecond(pair));
    }

    public static int length(Pattern pair) {
        if (pair == null) { throw new NullPointerException(); }
        if (!isList(pair)) { throw new IllegalArgumentException("pair must be a list"); }
        if (pair instanceof Nil) {
            return 0;
        } else {
            return 1 + length(Pair.getSecond(pair));
        }
    }

    //takes a pair and, treating it as a list, applies predicate to getFirst
    //if true returns true, else recurse on getSecond
    public static boolean contains(Pattern pair, Function<Pattern, Boolean> predicate) {
        if (pair == null) { throw new NullPointerException(); }
        return pair instanceof Pair && (predicate.apply(Pair.getFirst(pair)) || contains(Pair.getSecond(pair), predicate));
    }

}
