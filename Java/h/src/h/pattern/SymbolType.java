package h.pattern;

import h.model.HContext;

public class SymbolType extends Pattern {

    public static final SymbolType VALUE = new SymbolType();

    private SymbolType() {}

    @Override
    public boolean equals(Object other) {
        return other instanceof SymbolType;
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public String toString() {
        return "Symbol";
    }

    @Override
    public boolean isInfinite(HContext ctx) {
        return true;
    }

    @Override
    public int countValues(HContext ctx) {
        throw new UnsupportedOperationException("can't count the values of Symbol");
    }

}
