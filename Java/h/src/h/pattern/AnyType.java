package h.pattern;

import h.model.HContext;

public class AnyType extends Pattern {

    public static final AnyType VALUE = new AnyType();

    private AnyType() {}

    @Override
    public boolean equals(Object other) {
        return other != null && other instanceof AnyType;
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public String toString() {
        return "Any";
    }

    @Override
    public boolean isInfinite(HContext ctx) {
        return true;
    }

    @Override
    public int countValues(HContext ctx) {
        throw new UnsupportedOperationException("can't count the values of Any");
    }

}
