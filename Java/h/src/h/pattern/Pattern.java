package h.pattern;

import h.model.HContext;
import h.util.HException;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by colinholzman on 8/28/17.
 */
public class Pattern {

	//let objs override this to make nice printing
	public String toString(int depth) {
		return toString();
	}

    //return the same Base with binding patterns replaced by their pattern
	public Pattern unwrapBindingPattern() {
	    return this;
    }

    //expand a Pattern. e.g. ({1 2}.()).expand() == {(1.()) (2.())}
    public Pattern expand(HContext ctx) throws HException {
	    return this;
    }

    public java.util.Set<Pattern> toSet(HContext ctx) {
	    java.util.Set<Pattern> ret = new HashSet<>();
	    ret.add(this);
	    return ret;
    }

    //returns true if this pattern contains the pattern p for determining if a type constructor is recursive
    //i.e. {() (Integer .[List Integer])}.contains([List Integer]) is true
    //
    public boolean contains(Pattern p) {
	    return equals(p);
    }

    //return true if this pattern is a literal, e.g. 5, foo, (5 .foo) are literals
    //Integer, Boolean, Any are not literals
    public boolean isLiteral() {
	    return false;
    }

    //return true if this pattern is infinite, e.g. Any is infinite, Symbol is infinite, 5 is not infinite
    public boolean isInfinite(HContext ctx) throws HException {
	    return false;
    }

    //an enum to indicate if a pattern represents exactly
    //one value, one or more values, or infinitely many values.
    //Not really cardinality, but something similar
    public enum Cardinality {

        ZERO,       //the empty set   : {}
        ONE,        //literals        : 1 2 abc (2.abc)
        FINITE,     //finite options  : { 1 2 } (abc.{1 2})
        INFINITE;   //infinite options: Any Integer Symbol {Integer Symbol} (Any.2)

        public boolean less(Cardinality other) {
            return ordinal() < other.ordinal();
        }
    }

    //determine the cardinality of a pattern e.g.
    //{}.cardinality() == ZERO
    //5.cardinality() == FINITE
    //{2 3}.cardinality() == FINITE
    //Symbol.cardinality() == INFINITE
    public final Cardinality cardinality(HContext ctx) throws HException {
        if (this instanceof None) {
            //{} has no values
            return Cardinality.ZERO;
        } else if (isLiteral()) {
            //literals have only one value
            return Cardinality.ONE;
        } else if (isInfinite(ctx)) {
            //infinite patterns have infinite values
            return Cardinality.INFINITE;
        } else {
            //finite values
            return Cardinality.FINITE;
        }
    }

    //count the values in a pattern, e.g. 5.countValues() == 1
    //{}.countValues() == 0
    //{ 1 2 }.countValues() == 2
    public int countValues(HContext ctx) throws HException {
        return 0;
    }

    //return a flat version of self e.g.
    //5.flatten() == 5
    //(1 .2).flatten == (1 .2)
    //{ 1 2 }.flatten() == {1 2}
    //{1 {2 3}}.flatten() == { 1 2 3 }
    public Pattern flatten(HContext ctx) {
	    return this;
    }

    //return the normalized version of a pattern e.g.
    //43.normalize() == 42
    //(1 .2).normalize() == (1 .2)
    //{1 two Any}.normalize() == Any
    //{1 two Symbol}.normalize() == {1 Symbol}
    //{1 Integer}.normalize() == Integer
    public Pattern normalize(HContext ctx) {
	    return this;
    }

}
