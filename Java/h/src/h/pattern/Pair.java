package h.pattern;

import h.model.HContext;

import java.util.*;

/**
 * Created by colinholzman on 8/28/17.
 */
public class Pair extends Pattern {

    private Pattern first;

    public Pattern getFirst() {
        return first;
    }

    private Pattern second;

    public Pattern getSecond() {
        return second;
    }

    public Pair(Pattern first, Pattern second) {
        if (first == null || second == null) { throw new NullPointerException(); }
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean equals(Object other) {
        return (this == other
            || (other instanceof Pair
                && getFirst().equals(((Pair) other).getFirst())
                && getSecond().equals(((Pair) other).getSecond())));
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] {getFirst(), getSecond()});
    }

    @Override
    public Pattern unwrapBindingPattern() {
        return new Pair(getFirst().unwrapBindingPattern(), getSecond().unwrapBindingPattern());
    }

    private String toStringImpl(int depth) {
        StringBuilder ret = new StringBuilder("( " + getFirst().toString(depth + 1));
        Pattern next = getSecond();
        while (next instanceof Pair) {
            ret.append(" ").append(((Pair) next).getFirst().toString(depth + 1));
            next = ((Pair) next).getSecond();
        }
        if (!(next instanceof Nil)) {
            ret.append(" . ").append(next.toString(depth + 1));
        }
        ret.append(" )");
        return ret.toString();
    }

    @Override
    public String toString() {
        return toStringImpl(0);
    }

    @Override
    public String toString(int depth) {
        if (depth > 3) {
            return "(...)";
        } else {
            return toStringImpl(depth);
        }
    }

    @Override
    public Pattern expand(HContext ctx) {
        Pattern fstExpand = getFirst().expand(ctx).normalize(ctx);
        Pattern sndExpand = getSecond().expand(ctx).normalize(ctx);
        java.util.List<Pattern> result = new ArrayList<>();
        if (fstExpand instanceof Option && sndExpand instanceof Option) {
            ((Option) fstExpand).getElements().forEach(fst ->
                ((Option) sndExpand).getElements().forEach(snd ->
                    result.add(new Pair(fst, snd))
                )
            );
        } else if (fstExpand instanceof Option) {
            ((Option) fstExpand).getElements().forEach(fst ->
                result.add(new Pair(fst, sndExpand))
            );
        } else if (sndExpand instanceof Option) {
            ((Option) sndExpand).getElements().forEach(snd ->
                result.add(new Pair(fstExpand, snd))
            );
        } else {
            result.add(new Pair(fstExpand, sndExpand));
        }
        return new Option(result).normalize(ctx);
    }

    @Override
    public boolean contains(Pattern p) {
        return getFirst().contains(p) || getSecond().contains(p);
    }

    public static Pattern getFirst(Pattern pair) {
        if (pair == null) { throw new NullPointerException(); }
        if (!(pair instanceof Pair)) { throw new IllegalArgumentException("argument to getFirst must be a pair"); }
        return ((Pair)pair).getFirst();
    }

    public static Pattern getSecond(Pattern pair) {
        if (pair == null) { throw new NullPointerException(); }
        if (!(pair instanceof Pair)) { throw new IllegalArgumentException("argument to getSecond must be a pair"); }
        return ((Pair)pair).getSecond();
    }

    @Override
    public boolean isLiteral() {
        return getFirst().isLiteral() && getSecond().isLiteral();
    }

    @Override
    public boolean isInfinite(HContext ctx) {
        return getFirst().isInfinite(ctx) || getSecond().isInfinite(ctx);
    }

    @Override
    public int countValues(HContext ctx) {
        return getFirst().countValues(ctx) * getSecond().countValues(ctx);
    }

    @Override
    public Pattern flatten(HContext ctx) {
        return new Pair(getFirst().flatten(ctx), getSecond().flatten(ctx));
    }

    @Override
    public Pattern normalize(HContext ctx) {
        return new Pair(getFirst().normalize(ctx), getSecond().normalize(ctx));
    }

}
