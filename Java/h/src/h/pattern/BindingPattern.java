package h.pattern;

import h.model.HContext;

import java.util.*;

public class BindingPattern extends Pattern {

    private Symbol name;

    public Symbol getName() {
        return name;
    }

    private Pattern constraint;

    public Pattern getPattern() {
        return constraint;
    }

    public BindingPattern(Symbol id, Pattern constraint) {
        if (id == null || constraint == null) { throw new NullPointerException(); }
        if (constraint instanceof BindingPattern) {throw new IllegalArgumentException("constraint can't be a bound pattern");}
        this.name = id;
        this.constraint = constraint;
    }

    public BindingPattern(String id, Pattern constraint) {
        if (id == null || constraint == null) { throw new NullPointerException(); }
        this.name = new Symbol(id);
        this.constraint = constraint;
    }

    @Override
    public boolean equals(Object other) {
		return other instanceof BindingPattern
			&& getName().equals(((BindingPattern) other).getName())
			&& getPattern().equals(((BindingPattern) other).getPattern());
	}

	@Override
    public Pattern unwrapBindingPattern() {
        return getPattern();
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { getName(), getPattern() });
    }

    @Override
    public String toString() {
        return getName().toString() + ":" + getPattern().toString();
    }

    @Override
    public boolean isLiteral() {
        return getPattern().isLiteral();
    }

    @Override
    public boolean isInfinite(HContext ctx) {
        return getPattern().isInfinite(ctx);
    }

    @Override
    public int countValues(HContext ctx) {
        return getPattern().countValues(ctx);
    }


}
