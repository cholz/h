package h.pattern;

import h.model.HContext;

import java.math.BigInteger;

/**
 * Created by colinholzman on 8/28/17.
 */
public class Integer extends Pattern {

    private BigInteger value = new BigInteger("0");

    public BigInteger getValue() {
        return value;
    }

    public Integer(long value) {
        this.value = new BigInteger(String.valueOf(value));
    }

    public Integer(BigInteger value) {
        if (value == null) { throw new NullPointerException(); }
        this.value = value;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof Integer
			&& ((Integer) other).getValue().equals(getValue());
    }

    @Override
    public int hashCode() {
        return getValue().hashCode();
    }

    @Override
    public String toString() {
        return getValue().toString();
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public int countValues(HContext ctx) {
        return 1;
    }

}
