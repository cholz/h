package h.parsing;

import h.pattern.*;
import h.pattern.Integer;
import h.hBaseVisitor;
import h.hParser;
import h.util.HException;

/**
 * Created by colinholzman on 8/28/17.
 */
public class ExprVisitor extends hBaseVisitor<Pattern> {

    @Override
    public Pattern visitNaturalExpr(hParser.NaturalExprContext ctx) {
        return new Integer(java.lang.Integer.valueOf(ctx.NATURAL().getText()));
    }

    @Override
    public Pattern visitSymbolExpr(hParser.SymbolExprContext ctx) {
        return new Symbol(ctx.SYMBOL().getText());
    }

    @Override
    public Pattern visitListExpr(hParser.ListExprContext ctx) {
        Pattern ret = Nil.VALUE;
        for (int i = ctx.expr().size(); i > 0; --i) {
            ret = new Pair(new ExprVisitor().visit(ctx.expr(i - 1)), ret);
        }
        return ret;
    }

    @Override
    public Pattern visitPairExpr(hParser.PairExprContext ctx) {
        Pattern ret = Nil.VALUE;
        if (ctx.last != null) {
            if (ctx.expr().size() == 0) { throw new HException("parse error: "); }
            ret = new ExprVisitor().visit(ctx.last);
            for (int i = ctx.expr().size() - 1; i > 0; --i) {
                ret = new Pair(new ExprVisitor().visit(ctx.expr(i - 1)), ret);
            }
        } else {
            for (int i = ctx.expr().size(); i > 0; --i) {
                ret = new Pair(new ExprVisitor().visit(ctx.expr(i - 1)), ret);
            }
        }
        return ret;
    }
}
