package h.parsing;

import h.model.UserDefinedType;
import h.hBaseVisitor;
import h.hParser;
import h.util.HException;

import java.util.HashMap;
import java.util.Map;

public class UserDefinedTypeGatherer extends hBaseVisitor<Map<String, UserDefinedType>> {

    @Override
    public Map<String, UserDefinedType> visitH(hParser.HContext ctx) {
        Map<String, UserDefinedType> ret = new HashMap<>();
        for (hParser.H_ruleContext c: ctx.h_rule()) {
            if (c instanceof hParser.UserDefinedTypeRuleContext) {
                UserDefinedType type = new UserDefinedTypeVisitor().visit(c);
                if (ret.put(type.getName().getId(), type) != null) {
                    throw new HException("duplicate type " + type.toString());
                }
            }
        }
        return ret;
    }
}
