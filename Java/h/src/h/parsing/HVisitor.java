package h.parsing;

/**
 * Created by colinholzman on 8/28/17.
 */

import h.model.HContext;
import h.model.Rule;
import h.pattern.Pattern;
import h.pattern.List;
import h.pattern.Nil;
import h.model.UserDefinedType;
import h.hBaseVisitor;
import h.hParser;

import java.util.*;

public class HVisitor extends hBaseVisitor<HContext> {

    @Override
    public HContext visitH(hParser.HContext ctx) {
        java.util.Set<Rule> rules = new RuleGatherer().visit(ctx);
        Map<String, UserDefinedType> types = new UserDefinedTypeGatherer().visit(ctx);
        Pattern state = List.create(Nil.VALUE, Nil.VALUE);
        return new HContext(rules, types, state);
    }
}



