package h.parsing;

import h.*;
import h.pattern.*;
import h.model.Rule;

/**
 * Created by colinholzman on 8/28/17.
 */
public class RuleVisitor extends hBaseVisitor<Rule> {

    @Override
    public Rule visitBasicRule(hParser.BasicRuleContext ctx) {
        return new Rule(new PatternVisitor().visit(ctx.before), new PatternVisitor().visit(ctx.after));
    }

    @Override
    public Rule visitFunctionRule(hParser.FunctionRuleContext ctx) {

        //program transition
        Symbol pSymbol = new Symbol();
        Pattern programBeforePattern = new Pair(new PatternVisitor().visit(ctx.name), new BindingPattern(pSymbol, AnyType.VALUE));
        Pattern programAfterPattern = pSymbol;
        for (int i = ctx.body().expr().size(); i > 0; --i) {
            hParser.ExprContext bodyExpr = ctx.body().expr(i - 1);
            programAfterPattern = new Pair(new ExprVisitor().visit(bodyExpr), programAfterPattern);
        }

        //stack transition
        Symbol sSymbol = new Symbol();
        Pattern stackBeforePattern = new BindingPattern(sSymbol, AnyType.VALUE);
        for (int i = 0; i < ctx.args().pattern().size(); ++i) {
            hParser.PatternContext argsPattern = ctx.args().pattern(i);
            stackBeforePattern = new Pair(new PatternVisitor().visit(argsPattern), stackBeforePattern);
        }
        Pattern stackAfterPattern = sSymbol;

        //error transition
//        Symbol eSymbol = new Symbol();
//        Base errorBeforePattern = new BindingPattern(eSymbol, AnyType.VALUE);
//        Base errorAfterPattern = eSymbol;

        return new Rule(List.create(programBeforePattern, stackBeforePattern),
                List.create(programAfterPattern, stackAfterPattern));
    }

    @Override
    public Rule visitStackRule(hParser.StackRuleContext ctx) {

        //program
        Symbol pSymbol = new Symbol();
        Pattern programBeforePattern = new Pair(new PatternVisitor().visit(ctx.name), new BindingPattern(pSymbol, AnyType.VALUE));
        Pattern programAfterPattern = pSymbol;

        //stack
        Symbol sSymbol = new Symbol();
        Pattern stackBeforePattern = new BindingPattern(sSymbol, AnyType.VALUE);
        for (int i = 0; i < ctx.before.pattern().size(); ++i) {
            stackBeforePattern = new Pair(new PatternVisitor().visit(ctx.before.pattern(i)), stackBeforePattern);
        }
        Pattern stackAfterPattern = sSymbol;
        for (int i = 0; i < ctx.after.pattern().size(); ++i) {
            stackAfterPattern = new Pair(new PatternVisitor().visit(ctx.after.pattern(i)), stackAfterPattern);
        }

        //error
//        Symbol eSymbol = new Symbol();
//        Base errorBeforePattern = new BindingPattern(eSymbol, AnyType.VALUE);
//        Base errorAfterPattern = eSymbol;

        return new Rule(List.create(programBeforePattern, stackBeforePattern),
                List.create(programAfterPattern, stackAfterPattern));

    }

    @Override
    public Rule visitEntryPointRule(hParser.EntryPointRuleContext ctx) {
        Pattern programList = List.create(new Symbol("end"));
        for (int i = ctx.body().expr().size(); i > 0; --i) {
            programList = new Pair(new ExprVisitor().visit(ctx.body().expr(i - 1)), programList);
        }
        return new Rule(List.create(Nil.VALUE, Nil.VALUE), List.create(programList, Nil.VALUE));
    }

}
