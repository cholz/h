package h.parsing;

import h.model.Rule;
import h.hBaseVisitor;
import h.hParser;
import h.util.HException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class RuleGatherer extends hBaseVisitor<java.util.Set<Rule>> {

    @Override
    public java.util.Set<Rule> visitH(hParser.HContext ctx) {
        java.util.Set<Rule> ret = new HashSet<>();
        for (hParser.H_ruleContext c: ctx.h_rule()) {
            if (!(c instanceof hParser.UserDefinedTypeRuleContext)) {
                Rule newRule = new RuleVisitor().visit(c);
                if (!ret.add(newRule)) {
                    throw new HException("duplicate rule " + newRule.toString());
                }
            }
        }
        return ret;
    }
}
