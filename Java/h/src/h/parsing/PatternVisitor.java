package h.parsing;

import h.pattern.*;
import h.pattern.Integer;
import h.hBaseVisitor;
import h.hParser;
import h.util.HException;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by colinholzman on 8/28/17.
 */
public class PatternVisitor extends hBaseVisitor<Pattern> {

    @Override
    public Pattern visitNaturalPattern(hParser.NaturalPatternContext ctx) {
        return new Integer(java.lang.Integer.valueOf(ctx.NATURAL().getText()));
    }

    @Override
    public Pattern visitSymbolPattern(hParser.SymbolPatternContext ctx) {
        return new Symbol(ctx.SYMBOL().getText());
    }

    @Override
    public Pattern visitBindingPattern(hParser.BindingPatternContext ctx) {
        return new BindingPattern(ctx.SYMBOL().getText(), visit(ctx.pattern()));
    }

    @Override
    public Pattern visitTypenamePattern(hParser.TypenamePatternContext ctx) {
        if (ctx.TYPENAME().getText().equals("Any")) {
            return AnyType.VALUE;
        } else if (ctx.TYPENAME().getText().equals("Integer")) {
            return IntegerType.VALUE;
        } else if (ctx.TYPENAME().getText().equals("Symbol")) {
            return SymbolType.VALUE;
        } else {
            return new TypeConstructor(new Symbol(ctx.TYPENAME().getText()), new ArrayList<>());
        }
    }

    @Override
    public Pattern visitOptionPattern(hParser.OptionPatternContext ctx) {
        if (ctx.pattern().size() == 0) {
            return None.VALUE;
        } else {
            return new Option(ctx.pattern().stream().map(this::visit).collect(Collectors.toList()));
        }
    }

    @Override
    public Pattern visitListPattern(hParser.ListPatternContext ctx) {
        Pattern ret = Nil.VALUE;
        for (int i = ctx.pattern().size(); i > 0; --i) {
            ret = new Pair(new PatternVisitor().visit(ctx.pattern(i - 1)), ret);
        }
        return ret;
    }

    @Override
    public Pattern visitPairPattern(hParser.PairPatternContext ctx) {
        Pattern ret = Nil.VALUE;
        if (ctx.last != null) {
            if (ctx.pattern().size() == 0) { throw new HException("parse error: "); }
            ret = new PatternVisitor().visit(ctx.last);
            for (int i = ctx.pattern().size() - 1; i > 0; --i) {
                ret = new Pair(new PatternVisitor().visit(ctx.pattern(i - 1)), ret);
            }
        } else {
            for (int i = ctx.pattern().size(); i > 0; --i) {
                ret = new Pair(new PatternVisitor().visit(ctx.pattern(i - 1)), ret);
            }
        }
        return ret;
    }

    @Override
    public Pattern visitTypeConstructorPattern(hParser.TypeConstructorPatternContext ctx) {
        return new TypeConstructor(new Symbol(ctx.TYPENAME().getText()), ctx.pattern().stream().map(pCtx -> new PatternVisitor().visit(pCtx)).collect(Collectors.toList()));
    }

}
