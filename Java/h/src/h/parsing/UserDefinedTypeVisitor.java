package h.parsing;

        import h.pattern.Pattern;
        import h.pattern.Symbol;
        import h.model.UserDefinedType;
        import h.hBaseVisitor;
        import h.hParser;

        import java.util.ArrayList;
        import java.util.HashSet;
        import java.util.Set;
        import java.util.stream.Collectors;

public class UserDefinedTypeVisitor extends hBaseVisitor<UserDefinedType> {

    @Override
    public UserDefinedType visitUserDefinedTypeRule(hParser.UserDefinedTypeRuleContext ctx) {
        Symbol name = new Symbol(ctx.TYPENAME().getText());
        ArrayList<Symbol> variables = new ArrayList<>(ctx.SYMBOL().stream().map(sym -> new Symbol(sym.getText())).collect(Collectors.toList()));
        java.util.Set<Symbol> varSet = new HashSet<>();
        //ensure no duplicates
        variables.forEach(v -> {
            if (!varSet.add(v)) {
                throw new UnsupportedOperationException("duplicate type variable " + v.getId());
            }
        });
        Pattern pattern = new PatternVisitor().visit(ctx.pattern());
        return new UserDefinedType(name, variables, pattern);
    }

}
