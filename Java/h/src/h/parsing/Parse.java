package h.parsing;

import h.util.HException;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import h.model.HContext;
import h.pattern.Pattern;
import h.model.Rule;
import h.model.UserDefinedType;
import h.hLexer;
import h.hParser;

import java.io.*;

public final class Parse {

    private Parse() {}

    private static CharStream getInputStream(String inputString) {
        if (inputString == null) {throw new NullPointerException();}
        CodePointCharStream cs = CharStreams.fromString(inputString);
        return cs;
    }

    private static CharStream getInputStream(File inputFile) throws HException {
        if (inputFile == null) { throw new NullPointerException(); }
        try {
            return CharStreams.fromStream(new FileInputStream(inputFile));
        } catch (IOException e) {
            throw new HException(e.getLocalizedMessage());
        }
    }

    private static hParser getParser(CharStream inputStream) {
        hLexer lexer = new hLexer(inputStream);
//        lexer.removeErrorListeners();
//        lexer.addErrorListener(DescriptiveErrorListener.INSTANCE);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        hParser parser = new hParser(tokens);
//        parser.removeErrorListeners();
//        parser.addErrorListener(DescriptiveErrorListener.INSTANCE);
        parser.setErrorHandler(new BailErrorStrategy());
        return parser;
    }

    public static HContext sp(File inputFile) throws HException {
        ParseTree tree = getParser(getInputStream(inputFile)).h();
        return new HVisitor().visit(tree);
    }

    public static HContext sp(String inputString) throws HException {
        ParseTree tree = getParser(getInputStream(inputString)).h();
        return new HVisitor().visit(tree);
    }

    public static Rule rule(String inputString) {
        ParseTree tree = getParser(getInputStream(inputString)).h_rule();
        return new RuleVisitor().visit(tree);
    }

    public static Pattern pattern(String inputString) {
        ParseTree tree = getParser(getInputStream(inputString)).pattern();
        return new PatternVisitor().visit(tree);
    }

    public static Pattern expr(String inputString) {
        ParseTree tree = getParser(getInputStream(inputString)).expr();
        return new ExprVisitor().visit(tree);
    }

    public static Pattern expr(CharStream inputStream) {
        ParseTree tree = getParser(inputStream).expr();
        return new ExprVisitor().visit(tree);
    }

    public static UserDefinedType type(String inputString) {
        ParseTree tree = getParser(getInputStream(inputString)).h_rule();
        return new UserDefinedTypeVisitor().visit(tree);
    }

}
