package h.interpret;

import h.checking.Bind;
import h.checking.Build;
import h.checking.Match;
import h.model.Rule;
import h.model.HContext;
import h.model.State;
import h.pattern.Pattern;
import h.pattern.List;
import h.pattern.Pair;
import h.parsing.Parse;
import h.util.HException;

import java.math.BigInteger;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Interpret {

    public static void interpret(HContext ctx) throws HException {

        //extract rules
        java.util.Set<Rule> rules = ctx.getRules();
        //and state from sp
        Pattern state = ctx.getState();

        //loop indefinitely
        for (;;) {

            //System.out.println("interpret state: " + state.toString());

            //match rules to state
            java.util.List<Rule> matches = Match.strictMatch(ctx, rules, state).stream().sorted((l, r) -> Rule.specificityComparer(ctx, l, r)).collect(Collectors.toList());

            //System.out.print("interpret matches: ");
            //matches.forEach(r -> System.out.print(r.toString() + " "));
            //System.out.println();

            //if no matches then break
            if (matches.size() == 0) { break; }

            //get the first (most specific rule)
            Rule matched = matches.get(0);

            //possibleMatchBind variables
            Map<String, Pattern> bindings = Bind.bind(ctx, matched, state);

            //build set of new states, exapanded so binding works
            state = Build.build(matched, bindings).expand(ctx);

            //check for special rules and modify state accordingly
            if (matched.equals(Rule.IN_RULE)) {
                //get line from standard input
                String inputString = new Scanner(System.in).nextLine();
                //parse expr
                Pattern input = Parse.expr(inputString);
                //debug
                //System.out.println("IN_RULE: " + input.toString());
                //make a new stack
                Pattern stack = new Pair(input, Pair.getSecond(State.getStack(state)));
                //get existing program
                Pattern program = State.getProgram(state);
                //make new state with existing program and new stack
                state = List.create(program, stack);
            } else if (matched.equals(Rule.OUT_RULE)) {
                //just print the value that was bound by the out rule
                System.out.println(bindings.get("v").toString());
            } else if (matched.equals(Rule.INC_RULE)) {
                //increment the value that was bound by the inc rule
                Pattern newValue = new h.pattern.Integer(((h.pattern.Integer)bindings.get("i")).getValue().add(new BigInteger("1")));
                //make new stack with that value
                Pattern stack = new Pair(newValue, Pair.getSecond(State.getStack(state)));
                //get existing program
                Pattern program = State.getProgram(state);
                //make new state
                state = List.create(program, stack);
            } else if (matched.equals(Rule.DEC_RULE)) {
                //same as inc rule
                Pattern newValue = new h.pattern.Integer(((h.pattern.Integer)bindings.get("i")).getValue().subtract(new BigInteger("1")));
                Pattern stack = new Pair(newValue, Pair.getSecond(State.getStack(state)));
                Pattern program = State.getProgram(state);
                state = List.create(program, stack);
            }

        }
        //System.out.println("no match");
    }

}
