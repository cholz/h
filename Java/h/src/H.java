import h.interpret.Interpret;
import h.model.UserDefinedType;
import h.util.Cli;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import h.model.HContext;
import h.checking.Check;
import h.parsing.Parse;
import h.model.Rule;

import java.io.File;
import java.util.Map;
import java.util.Set;

public class H {

    public static void main(String[] args) {

        args = new String[] { "-i", "C:\\code_projects\\sp\\sp\\complete.sp" };

        new Cli(args).parse();

    }

}