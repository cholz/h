// Generated from C:/code_projects/sp/grammar\h.g4 by ANTLR 4.7
package h;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link hParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface hVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link hParser#h}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitH(hParser.HContext ctx);
	/**
	 * Visit a parse tree produced by {@link hParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(hParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link hParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs(hParser.ArgsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code basicRule}
	 * labeled alternative in {@link hParser#h_rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBasicRule(hParser.BasicRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stackRule}
	 * labeled alternative in {@link hParser#h_rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStackRule(hParser.StackRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionRule}
	 * labeled alternative in {@link hParser#h_rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionRule(hParser.FunctionRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code entryPointRule}
	 * labeled alternative in {@link hParser#h_rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEntryPointRule(hParser.EntryPointRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code userDefinedTypeRule}
	 * labeled alternative in {@link hParser#h_rule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUserDefinedTypeRule(hParser.UserDefinedTypeRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code naturalPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNaturalPattern(hParser.NaturalPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code symbolPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSymbolPattern(hParser.SymbolPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bindingPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBindingPattern(hParser.BindingPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typenamePattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypenamePattern(hParser.TypenamePatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code optionPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptionPattern(hParser.OptionPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeConstructorPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeConstructorPattern(hParser.TypeConstructorPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListPattern(hParser.ListPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pairPattern}
	 * labeled alternative in {@link hParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPairPattern(hParser.PairPatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code naturalExpr}
	 * labeled alternative in {@link hParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNaturalExpr(hParser.NaturalExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code symbolExpr}
	 * labeled alternative in {@link hParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSymbolExpr(hParser.SymbolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code listExpr}
	 * labeled alternative in {@link hParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListExpr(hParser.ListExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pairExpr}
	 * labeled alternative in {@link hParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPairExpr(hParser.PairExprContext ctx);
}