// Generated from C:/code_projects/sp/grammar\h.g4 by ANTLR 4.7
package h;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class hParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, SYMBOL=12, TYPENAME=13, NATURAL=14, WHITESPACE=15, 
		COMMENT=16;
	public static final int
		RULE_h = 0, RULE_body = 1, RULE_args = 2, RULE_h_rule = 3, RULE_pattern = 4, 
		RULE_expr = 5;
	public static final String[] ruleNames = {
		"h", "body", "args", "h_rule", "pattern", "expr"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "'.'", "'->'", "')'", "'='", "':'", "'>'", "'['", "']'", 
		"'{'", "'}'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		"SYMBOL", "TYPENAME", "NATURAL", "WHITESPACE", "COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "h.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public hParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class HContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(hParser.EOF, 0); }
		public List<H_ruleContext> h_rule() {
			return getRuleContexts(H_ruleContext.class);
		}
		public H_ruleContext h_rule(int i) {
			return getRuleContext(H_ruleContext.class,i);
		}
		public HContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_h; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitH(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HContext h() throws RecognitionException {
		HContext _localctx = new HContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_h);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(15);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(12);
				h_rule();
				}
				}
				setState(17);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(18);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << SYMBOL) | (1L << NATURAL))) != 0)) {
				{
				{
				setState(20);
				expr();
				}
				}
				setState(25);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_args);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__7) | (1L << T__9) | (1L << SYMBOL) | (1L << TYPENAME) | (1L << NATURAL))) != 0)) {
				{
				{
				setState(26);
				pattern();
				}
				}
				setState(31);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class H_ruleContext extends ParserRuleContext {
		public H_ruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_h_rule; }
	 
		public H_ruleContext() { }
		public void copyFrom(H_ruleContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BasicRuleContext extends H_ruleContext {
		public PatternContext before;
		public PatternContext after;
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public BasicRuleContext(H_ruleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitBasicRule(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EntryPointRuleContext extends H_ruleContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public EntryPointRuleContext(H_ruleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitEntryPointRule(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StackRuleContext extends H_ruleContext {
		public ArgsContext before;
		public PatternContext name;
		public ArgsContext after;
		public List<ArgsContext> args() {
			return getRuleContexts(ArgsContext.class);
		}
		public ArgsContext args(int i) {
			return getRuleContext(ArgsContext.class,i);
		}
		public PatternContext pattern() {
			return getRuleContext(PatternContext.class,0);
		}
		public StackRuleContext(H_ruleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitStackRule(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UserDefinedTypeRuleContext extends H_ruleContext {
		public TerminalNode TYPENAME() { return getToken(hParser.TYPENAME, 0); }
		public PatternContext pattern() {
			return getRuleContext(PatternContext.class,0);
		}
		public List<TerminalNode> SYMBOL() { return getTokens(hParser.SYMBOL); }
		public TerminalNode SYMBOL(int i) {
			return getToken(hParser.SYMBOL, i);
		}
		public UserDefinedTypeRuleContext(H_ruleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitUserDefinedTypeRule(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionRuleContext extends H_ruleContext {
		public PatternContext name;
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public PatternContext pattern() {
			return getRuleContext(PatternContext.class,0);
		}
		public FunctionRuleContext(H_ruleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitFunctionRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final H_ruleContext h_rule() throws RecognitionException {
		H_ruleContext _localctx = new H_ruleContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_h_rule);
		int _la;
		try {
			setState(77);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new BasicRuleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(32);
				match(T__0);
				setState(33);
				match(T__1);
				setState(34);
				((BasicRuleContext)_localctx).before = pattern();
				setState(35);
				match(T__2);
				setState(36);
				((BasicRuleContext)_localctx).after = pattern();
				setState(37);
				match(T__3);
				}
				break;
			case 2:
				_localctx = new StackRuleContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(39);
				match(T__0);
				setState(40);
				match(T__4);
				setState(41);
				match(T__0);
				setState(42);
				((StackRuleContext)_localctx).before = args();
				setState(43);
				match(T__3);
				setState(44);
				((StackRuleContext)_localctx).name = pattern();
				setState(45);
				match(T__0);
				setState(46);
				((StackRuleContext)_localctx).after = args();
				setState(47);
				match(T__3);
				setState(48);
				match(T__3);
				}
				break;
			case 3:
				_localctx = new FunctionRuleContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(50);
				match(T__0);
				setState(51);
				match(T__5);
				setState(52);
				match(T__0);
				setState(53);
				args();
				setState(54);
				match(T__3);
				setState(55);
				((FunctionRuleContext)_localctx).name = pattern();
				setState(56);
				body();
				setState(57);
				match(T__3);
				}
				break;
			case 4:
				_localctx = new EntryPointRuleContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(59);
				match(T__0);
				setState(60);
				match(T__6);
				setState(61);
				body();
				setState(62);
				match(T__3);
				}
				break;
			case 5:
				_localctx = new UserDefinedTypeRuleContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(64);
				match(T__0);
				setState(65);
				match(T__7);
				setState(66);
				match(TYPENAME);
				setState(70);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==SYMBOL) {
					{
					{
					setState(67);
					match(SYMBOL);
					}
					}
					setState(72);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(73);
				match(T__8);
				setState(74);
				pattern();
				setState(75);
				match(T__3);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PatternContext extends ParserRuleContext {
		public PatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern; }
	 
		public PatternContext() { }
		public void copyFrom(PatternContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ListPatternContext extends PatternContext {
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public ListPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitListPattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeConstructorPatternContext extends PatternContext {
		public TerminalNode TYPENAME() { return getToken(hParser.TYPENAME, 0); }
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public TypeConstructorPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitTypeConstructorPattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SymbolPatternContext extends PatternContext {
		public TerminalNode SYMBOL() { return getToken(hParser.SYMBOL, 0); }
		public SymbolPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitSymbolPattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypenamePatternContext extends PatternContext {
		public TerminalNode TYPENAME() { return getToken(hParser.TYPENAME, 0); }
		public TypenamePatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitTypenamePattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BindingPatternContext extends PatternContext {
		public TerminalNode SYMBOL() { return getToken(hParser.SYMBOL, 0); }
		public PatternContext pattern() {
			return getRuleContext(PatternContext.class,0);
		}
		public BindingPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitBindingPattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OptionPatternContext extends PatternContext {
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public OptionPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitOptionPattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PairPatternContext extends PatternContext {
		public PatternContext last;
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public PairPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitPairPattern(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NaturalPatternContext extends PatternContext {
		public TerminalNode NATURAL() { return getToken(hParser.NATURAL, 0); }
		public NaturalPatternContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitNaturalPattern(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PatternContext pattern() throws RecognitionException {
		PatternContext _localctx = new PatternContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_pattern);
		int _la;
		try {
			setState(120);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new NaturalPatternContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(79);
				match(NATURAL);
				}
				break;
			case 2:
				_localctx = new SymbolPatternContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(80);
				match(SYMBOL);
				}
				break;
			case 3:
				_localctx = new BindingPatternContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(81);
				match(SYMBOL);
				setState(82);
				match(T__5);
				setState(83);
				pattern();
				}
				break;
			case 4:
				_localctx = new TypenamePatternContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(84);
				match(TYPENAME);
				}
				break;
			case 5:
				_localctx = new OptionPatternContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(85);
				match(T__9);
				setState(89);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__7) | (1L << T__9) | (1L << SYMBOL) | (1L << TYPENAME) | (1L << NATURAL))) != 0)) {
					{
					{
					setState(86);
					pattern();
					}
					}
					setState(91);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(92);
				match(T__10);
				}
				break;
			case 6:
				_localctx = new TypeConstructorPatternContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(93);
				match(T__7);
				setState(94);
				match(TYPENAME);
				setState(98);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__7) | (1L << T__9) | (1L << SYMBOL) | (1L << TYPENAME) | (1L << NATURAL))) != 0)) {
					{
					{
					setState(95);
					pattern();
					}
					}
					setState(100);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(101);
				match(T__8);
				}
				break;
			case 7:
				_localctx = new ListPatternContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(102);
				match(T__0);
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__7) | (1L << T__9) | (1L << SYMBOL) | (1L << TYPENAME) | (1L << NATURAL))) != 0)) {
					{
					{
					setState(103);
					pattern();
					}
					}
					setState(108);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(109);
				match(T__3);
				}
				break;
			case 8:
				_localctx = new PairPatternContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(110);
				match(T__0);
				setState(112); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(111);
					pattern();
					}
					}
					setState(114); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__7) | (1L << T__9) | (1L << SYMBOL) | (1L << TYPENAME) | (1L << NATURAL))) != 0) );
				setState(116);
				match(T__1);
				setState(117);
				((PairPatternContext)_localctx).last = pattern();
				setState(118);
				match(T__3);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NaturalExprContext extends ExprContext {
		public TerminalNode NATURAL() { return getToken(hParser.NATURAL, 0); }
		public NaturalExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitNaturalExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SymbolExprContext extends ExprContext {
		public TerminalNode SYMBOL() { return getToken(hParser.SYMBOL, 0); }
		public SymbolExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitSymbolExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PairExprContext extends ExprContext {
		public ExprContext last;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public PairExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitPairExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ListExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ListExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof hVisitor ) return ((hVisitor<? extends T>)visitor).visitListExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_expr);
		int _la;
		try {
			setState(142);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				_localctx = new NaturalExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(122);
				match(NATURAL);
				}
				break;
			case 2:
				_localctx = new SymbolExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(123);
				match(SYMBOL);
				}
				break;
			case 3:
				_localctx = new ListExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(124);
				match(T__0);
				setState(128);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << SYMBOL) | (1L << NATURAL))) != 0)) {
					{
					{
					setState(125);
					expr();
					}
					}
					setState(130);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(131);
				match(T__3);
				}
				break;
			case 4:
				_localctx = new PairExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(132);
				match(T__0);
				setState(134); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(133);
					expr();
					}
					}
					setState(136); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << SYMBOL) | (1L << NATURAL))) != 0) );
				setState(138);
				match(T__1);
				setState(139);
				((PairExprContext)_localctx).last = expr();
				setState(140);
				match(T__3);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\22\u0093\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\7\2\20\n\2\f\2\16\2\23\13"+
		"\2\3\2\3\2\3\3\7\3\30\n\3\f\3\16\3\33\13\3\3\4\7\4\36\n\4\f\4\16\4!\13"+
		"\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\7\5G\n\5\f\5\16\5J\13\5\3\5\3\5\3\5\3\5\5\5P\n\5\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\7\6Z\n\6\f\6\16\6]\13\6\3\6\3\6\3\6\3\6\7\6c\n\6\f"+
		"\6\16\6f\13\6\3\6\3\6\3\6\7\6k\n\6\f\6\16\6n\13\6\3\6\3\6\3\6\6\6s\n\6"+
		"\r\6\16\6t\3\6\3\6\3\6\3\6\5\6{\n\6\3\7\3\7\3\7\3\7\7\7\u0081\n\7\f\7"+
		"\16\7\u0084\13\7\3\7\3\7\3\7\6\7\u0089\n\7\r\7\16\7\u008a\3\7\3\7\3\7"+
		"\3\7\5\7\u0091\n\7\3\7\2\2\b\2\4\6\b\n\f\2\2\2\u00a4\2\21\3\2\2\2\4\31"+
		"\3\2\2\2\6\37\3\2\2\2\bO\3\2\2\2\nz\3\2\2\2\f\u0090\3\2\2\2\16\20\5\b"+
		"\5\2\17\16\3\2\2\2\20\23\3\2\2\2\21\17\3\2\2\2\21\22\3\2\2\2\22\24\3\2"+
		"\2\2\23\21\3\2\2\2\24\25\7\2\2\3\25\3\3\2\2\2\26\30\5\f\7\2\27\26\3\2"+
		"\2\2\30\33\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2\2\32\5\3\2\2\2\33\31\3\2"+
		"\2\2\34\36\5\n\6\2\35\34\3\2\2\2\36!\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2"+
		" \7\3\2\2\2!\37\3\2\2\2\"#\7\3\2\2#$\7\4\2\2$%\5\n\6\2%&\7\5\2\2&\'\5"+
		"\n\6\2\'(\7\6\2\2(P\3\2\2\2)*\7\3\2\2*+\7\7\2\2+,\7\3\2\2,-\5\6\4\2-."+
		"\7\6\2\2./\5\n\6\2/\60\7\3\2\2\60\61\5\6\4\2\61\62\7\6\2\2\62\63\7\6\2"+
		"\2\63P\3\2\2\2\64\65\7\3\2\2\65\66\7\b\2\2\66\67\7\3\2\2\678\5\6\4\28"+
		"9\7\6\2\29:\5\n\6\2:;\5\4\3\2;<\7\6\2\2<P\3\2\2\2=>\7\3\2\2>?\7\t\2\2"+
		"?@\5\4\3\2@A\7\6\2\2AP\3\2\2\2BC\7\3\2\2CD\7\n\2\2DH\7\17\2\2EG\7\16\2"+
		"\2FE\3\2\2\2GJ\3\2\2\2HF\3\2\2\2HI\3\2\2\2IK\3\2\2\2JH\3\2\2\2KL\7\13"+
		"\2\2LM\5\n\6\2MN\7\6\2\2NP\3\2\2\2O\"\3\2\2\2O)\3\2\2\2O\64\3\2\2\2O="+
		"\3\2\2\2OB\3\2\2\2P\t\3\2\2\2Q{\7\20\2\2R{\7\16\2\2ST\7\16\2\2TU\7\b\2"+
		"\2U{\5\n\6\2V{\7\17\2\2W[\7\f\2\2XZ\5\n\6\2YX\3\2\2\2Z]\3\2\2\2[Y\3\2"+
		"\2\2[\\\3\2\2\2\\^\3\2\2\2][\3\2\2\2^{\7\r\2\2_`\7\n\2\2`d\7\17\2\2ac"+
		"\5\n\6\2ba\3\2\2\2cf\3\2\2\2db\3\2\2\2de\3\2\2\2eg\3\2\2\2fd\3\2\2\2g"+
		"{\7\13\2\2hl\7\3\2\2ik\5\n\6\2ji\3\2\2\2kn\3\2\2\2lj\3\2\2\2lm\3\2\2\2"+
		"mo\3\2\2\2nl\3\2\2\2o{\7\6\2\2pr\7\3\2\2qs\5\n\6\2rq\3\2\2\2st\3\2\2\2"+
		"tr\3\2\2\2tu\3\2\2\2uv\3\2\2\2vw\7\4\2\2wx\5\n\6\2xy\7\6\2\2y{\3\2\2\2"+
		"zQ\3\2\2\2zR\3\2\2\2zS\3\2\2\2zV\3\2\2\2zW\3\2\2\2z_\3\2\2\2zh\3\2\2\2"+
		"zp\3\2\2\2{\13\3\2\2\2|\u0091\7\20\2\2}\u0091\7\16\2\2~\u0082\7\3\2\2"+
		"\177\u0081\5\f\7\2\u0080\177\3\2\2\2\u0081\u0084\3\2\2\2\u0082\u0080\3"+
		"\2\2\2\u0082\u0083\3\2\2\2\u0083\u0085\3\2\2\2\u0084\u0082\3\2\2\2\u0085"+
		"\u0091\7\6\2\2\u0086\u0088\7\3\2\2\u0087\u0089\5\f\7\2\u0088\u0087\3\2"+
		"\2\2\u0089\u008a\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b"+
		"\u008c\3\2\2\2\u008c\u008d\7\4\2\2\u008d\u008e\5\f\7\2\u008e\u008f\7\6"+
		"\2\2\u008f\u0091\3\2\2\2\u0090|\3\2\2\2\u0090}\3\2\2\2\u0090~\3\2\2\2"+
		"\u0090\u0086\3\2\2\2\u0091\r\3\2\2\2\17\21\31\37HO[dltz\u0082\u008a\u0090";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}