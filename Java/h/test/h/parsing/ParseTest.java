package h.parsing;

import org.junit.Test;
import h.pattern.*;
import h.pattern.Integer;
import h.model.Rule;

public class ParseTest {

// pattern
// : NATURAL #naturalPattern
// | SYMBOL COLON interval #constraintPattern
// | interval #intervalPattern
// | SYMBOL #symbolPattern
// | QUOTE SYMBOL #quotePattern
// | LPAREN pattern* (DOT last=pattern)? RPAREN #listPattern
// ;

    @Test
    public void test() {
        assert (new Integer(0) .equals(Parse.pattern("0")));
        assert (new Integer(0) .equals(Parse.expr("0")));
        assert (new Integer(1) .equals(Parse.pattern("1")));
        assert (new Integer(1000000000) .equals(Parse.pattern("1000000000")));
        assert (new Integer(1000000000) .equals(Parse.expr("1000000000")));
        assert (new Symbol("a") .equals(Parse.pattern("a")));
        assert (new Symbol("a") .equals(Parse.expr("a")));
        assert (new Symbol("abcdef_GHIJK") .equals(Parse.pattern("abcdef_GHIJK")));
        assert (new Symbol("abcdef_GHIJK") .equals(Parse.expr("abcdef_GHIJK")));
        assert (None.VALUE.equals(Parse.pattern("{}")));
        assert (new Option(new Integer(1), new Integer(2)).equals(Parse.pattern("{ 2 1 }")));
        assert (new Option(new Integer(1), new Integer(2)).equals(Parse.pattern("{ 1 2 }")));
        assert (new Option(new Option(new Integer(3), new Integer(1)), new Integer(2)).equals(Parse.pattern("{ 2 {1 3} }")));
        assert (Nil.VALUE .equals(Parse.pattern("()")));
        assert (Nil.VALUE .equals(Parse.expr("()")));
        assert (Nil.VALUE .equals(Parse.pattern("( )")));
        assert (Nil.VALUE .equals(Parse.expr("( )")));
        assert (new Pair(Nil.VALUE, Nil.VALUE) .equals(Parse.pattern("(().())")));
        assert (new Pair(Nil.VALUE, Nil.VALUE) .equals(Parse.expr("(().())")));
        assert (new Pair(Nil.VALUE, Nil.VALUE) .equals(Parse.pattern("( () . () )")));
        assert (new Pair(Nil.VALUE, Nil.VALUE) .equals(Parse.expr("( () . () )")));
        assert (new Pair(Nil.VALUE, new Pair(Nil.VALUE, Nil.VALUE)) .equals(Parse.pattern("(().(().()))")));
        assert (new Pair(Nil.VALUE, new Pair(Nil.VALUE, Nil.VALUE)) .equals(Parse.pattern("(() () .())")));
        assert (new Option(new Integer(1), new Integer(2)).equals(Parse.pattern("{1 2}")));
        assert (AnyType.VALUE.equals(Parse.pattern("Any")));
        assert (IntegerType.VALUE.equals(Parse.pattern("Integer")));
        assert (SymbolType.VALUE.equals(Parse.pattern("Symbol")));
        assert (new Rule(List.create(Nil.VALUE, Nil.VALUE), List.create(Nil.VALUE, Nil.VALUE)).equals(Parse.rule("(. (() ()) -> (() ()))")));
        assert (Parse.rule("(> () 1 two (three.4) )").equals(Parse.rule("(. (() ()) -> ( ( () 1 two (three.4) end ) () ) )")) );
    }

}
