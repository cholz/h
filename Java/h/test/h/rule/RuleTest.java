package h.rule;

import h.model.HContext;
import junit.framework.TestCase;
import org.junit.Test;
import h.model.Rule;
import h.parsing.Parse;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class RuleTest {

    @Test
    public void testIsMoreSpecific() {
        String pairs[] = new String[] {
            //the default transitions for the rule on the right use any, lhs should be more specific
            "(= ( () ) foo ())", "(= (Any) foo () )",
            //this is the empty create case of the i combinator, it should be more specific than the one item case
            "(: ( () ) i () )", "(: ( (f:Any.()) ) i () )",
            //and it should be more specific than the arbitrary pair case
            "(: ( () ) i () )", "(: ( (f:Any .r:Any) ) i () )",
            //similarly the single item case is more specific than the arbitrary pair case
            "(: ( (f:Any.()) ) i () )", "(: ( (f:Any .r:Any) ) i () )",
            "(: (l:(Any .Any)) count 0 l countImpl )", "(: (Any) count 0 )"
        };
        for (int i = 0; i < pairs.length; i += 2) {
            assertTrue(pairs[i] + " should be more specific than " + pairs[i + 1],
                    Parse.rule(pairs[i]).isMoreSpecific(HContext.DEFAULT_CONTEXT, Parse.rule(pairs[i+1])));
        }
    }

    @Test
    public void testUnion() {

        Object[] pairs = new Object[] {
            new String[]{"(. 1 -> () )", }, "(. 1 -> () )",
            new String[]{"(. 1 -> () )", "(. 2 -> () )"}, "(. { 1 2 } -> () )",
            new String[]{"(. (1 .two) -> () )", "(. (2 .three) -> () )"}, "(. {(1.two) (2.three)} -> () )",
            new String[]{"(. { 1 2 } -> () )", "(. { 3 4 } -> () )"}, "(. { 1 2 3 4 } -> () )",
            new String[]{"(. 1 -> () )", "(. { 3 4 } -> () )"}, "(. { 1 3 4 } -> () )",
            new String[]{"(. {} -> () )", "(. { 3 4 } -> () )"}, "(. { 3 4 } -> () )",
            new String[]{"(. {a a} -> () )", "(. { 3 4 } -> () )"}, "(. { a 3 4 } -> () )",
            new String[]{"(. {Any} -> () )", "(. { 3 4 } -> () )"}, "(. Any -> () )",
            new String[]{"(. {Any} -> () )", "(. {Symbol} -> () )"}, "(. Any -> () )",
            new String[]{"(. (3.4) -> () )", "(. (1.2) -> () )"}, "(. {(1.2) (3.4)} -> () )",
            new String[]{"(. (3.4) -> () )", "(. (1.2) -> () )", "(. (5.6) -> () )"}, "(. {(1.2) (3.4) (5.6)} -> () )",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            java.util.Set<Rule> rules = Arrays.stream((String[]) pairs[i]).map(Parse::rule).collect(Collectors.toSet());
            Rule union = Rule.union(HContext.DEFAULT_CONTEXT, rules);
            Rule result = Parse.rule((String)pairs[i+1]);
            TestCase.assertTrue(union + " should equal " + result, union.equals(result));
        }
    }

}
