package h.model;

import org.junit.Test;
import h.model.HContext;
import h.pattern.Pattern;
import h.parsing.Parse;
import h.model.Rule;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;

public class HContextTest {

    @Test
    public void testSp() {
        java.util.Set<Rule> rules = new HashSet<>();
        Pattern state = Parse.pattern("(()()())");
        HContext sp = new HContext(rules, new HashMap<>(), state);
        assertTrue("Sp::getRules()", sp.getRules().equals(rules));
        assertTrue("Sp::getState()", sp.getState().equals(state));
    }

}
