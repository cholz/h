package h.checking;

import org.junit.Test;
import h.pattern.*;
import h.pattern.Integer;
import h.parsing.Parse;
import h.model.Rule;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class BuildTest {

    @Test
    public void testBuild() {

        //no bindings
        Map<String, Pattern> bindings = new HashMap<>();
        Object pairs[] = new Object[] {
                "0", "0",
                "a", "a",
                "()", "()",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Pattern built = Build.build(Parse.pattern((String)pairs[i]), bindings);
            final Pattern result = Parse.pattern((String)pairs[i + 1]);
            assertTrue(pairs[i] + " should have built " + pairs[i + 1], built.equals(result));
        }

        //basic bindings
        bindings.put("a", new Integer(0));
        bindings.put("b", new Symbol("true"));
        bindings.put("c", Nil.VALUE);
        pairs = new Object[] {
                "(a b c)", "(0 true ())",
                "(b c a)", "(true () 0)",
                "(c a b)", "(() 0 true)",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Pattern built = Build.build(Parse.pattern((String)pairs[i]), bindings);
            final Pattern result = Parse.pattern((String)pairs[i + 1]);
            assertTrue(pairs[i] + " should have built " + pairs[i + 1], built.equals(result));
        }

        //replace a with an option of two values, each pattern should build a set of two values
        bindings.put("a", new Option(new Integer(0), new Integer(1)));
        pairs = new Object[] {
                "(a b c)", "({0 1} true ())",
                "(b c a)", "(true () {0 1})",
                "(c a b)", "(() {0 1} true)",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Pattern built = Build.build(Parse.pattern((String)pairs[i]), bindings);
            Pattern result = Parse.pattern(((String) pairs[i + 1]));
            assertTrue(pairs[i] + " should have built " + result, built.equals(result));
        }

        //remove the binding for a
        bindings.remove("a");
        pairs = new Object[] {
                "({0 1} b c)", "({0 1} true ())",
                "(b c {0 1})", "(true () {0 1})",
                "(c {0 1} b)", "(() {0 1} true)",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Pattern built = Build.build(Parse.pattern((String)pairs[i]), bindings);
            Pattern result = Parse.pattern(((String) pairs[i + 1]));
            assertTrue(pairs[i] + " should have built " + result, built.equals(result));
        }

        //replace a with an option of two values, each pattern should build a set of two values
        bindings.put("a", new Option(new Integer(0), new Integer(1)));
        pairs = new Object[] {
                "(. (() () ()) -> (a b c) )", "({0 1} true ())",
                "(. (() () ()) -> (b c a) )", "(true () {0 1})",
                "(. (() () ()) -> (c a b) )", "(() {0 1} true)",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Rule rule = Parse.rule((String)pairs[i]);
            Pattern built = Build.build(rule, bindings);
            Pattern result = Parse.pattern((String)pairs[i + 1]);
            assertTrue(pairs[i] + " should have built " + result, built.equals(result));
        }

        //remove the binding for a
        bindings.remove("a");
        pairs = new Object[] {
                "(. (() () ()) -> ({0 1} b c) )", "({0 1} true ())",
                "(. (() () ()) -> (b c {0 1}) )", "(true () {0 1})",
                "(. (() () ()) -> (c {0 1} b) )", "(() {0 1} true)",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Pattern built = Build.build(Parse.rule((String)pairs[i]), bindings);
            Pattern result = Parse.pattern((String) pairs[i + 1]);
            assertTrue(pairs[i] + " should have built " + result, built.equals(result));
        }
    }
}
