package h.checking;

import h.model.HContext;
import org.junit.Test;
import h.pattern.Pattern;
import h.parsing.Parse;
import h.model.Rule;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class MatchTest {

    @Test
    public void strictMatch() {
        String pairs[] = new String[] {
//            "(. { (1.3) (2.3) } -> ())", "({1 2}.3)",
            "(. (() () ()) -> () )",                    "(() () ())",
            "(. ( (i .Any) (() .Any) .e:Any ) -> () )", "((i) (().()) ())",
            "(. ( (i .Any) (Any .Any) .e:Any ) -> () )","((i) ((().()).()) ())",
            "(. Symbol -> () )","abc",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Rule rule = Parse.rule(pairs[i]);
            Pattern state = Parse.pattern(pairs[i + 1]);
            assertTrue(rule + " should strict match " + state, Match.strictMatch(HContext.DEFAULT_CONTEXT, rule, state));
        }
        pairs = new String[] {
            "(: (0 a:Integer) factImpl a )", "( (factImpl.()) (Integer Integer) () )",
            "(. {} -> () )", "{}",
            "(. Any -> () )", "{}",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Rule rule = Parse.rule(pairs[i]);
            Pattern state = Parse.pattern(pairs[i + 1]);
            assertFalse(rule + " should not strict match " + state, Match.strictMatch(HContext.DEFAULT_CONTEXT, rule, state));
        }
    }

    @Test
    public void abstractMatch() {
        String pairs[] = new String[] {
            "(. { (1.3) (2.3) } -> ())",         "({1 2}.3)",
            "(. (() ()) -> () )",                "( () () )",
            "(. ( (i .Any) (() .Any) ) -> () )", "( (i) (().()))",
            "(. ( (i .Any) (Any .Any) ) -> () )","( (i) ((().()).()) )",
            "(: (0 a:Integer) factImpl a )", "( (factImpl.()) (Integer Integer) )",
            "(: (Any)            count (error) fst )", "((count) (Any))",
            "(: (l:(Any .Any))   count 0 l countImpl )", "((count) (Any))",
            "(: (())             count 0 )", "((count) (Any))",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Rule rule = Parse.rule(pairs[i]);
            Pattern state = Parse.pattern(pairs[i + 1]);
            assertTrue(rule + " should abstract match " + state, Match.possibleMatch(HContext.DEFAULT_CONTEXT, rule, state));
        }
    }

}
