package h.checking;

import h.model.HContext;
import org.junit.Test;
import h.pattern.*;
import h.pattern.Integer;
import h.parsing.Parse;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class BindTest {

    @Test
    public void testBind() {
        Map<String, Pattern> boundObjects = new HashMap<>();
        Object pairs[] = new Object[] {
                "0", "0",
                "a", "a",
                "()", "()",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            assertTrue(pairs[i] + " should not have bound anything to the pattern " + pairs[i + 1],
                Bind.bind(HContext.DEFAULT_CONTEXT, Parse.pattern((String)pairs[i]), Parse.pattern((String)pairs[i + 1])).equals(boundObjects));
        }
        boundObjects.put("a", new Integer(0));
        boundObjects.put("b", new Symbol("true"));
        boundObjects.put("c", Nil.VALUE);
        pairs = new Object[] {
                "(a:{0 1} b:{true false} .c:())", "(0 true .())",
                "(a:Integer b:Symbol .c:Any)", "(0 true .())",
                "(a:Any b:{(Any.Any) true} .c:())", "(0 true .())",
        };
        for (int i = 0; i < pairs.length; i += 2) {
            assertTrue(pairs[i] + " should have bound a b c to the pattern " + pairs[i + 1],
                Bind.bind(HContext.DEFAULT_CONTEXT, Parse.pattern((String)pairs[i]), Parse.pattern((String)pairs[i + 1])).equals(boundObjects));
        }
    }

}
