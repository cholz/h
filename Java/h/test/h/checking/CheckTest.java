package h.checking;

import h.model.HContext;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.DefaultGraph;
import org.junit.Test;
import h.pattern.Pattern;
import h.parsing.Parse;
import h.model.Rule;

import java.util.HashSet;
import java.util.Set;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckTest {

    @Test
    public void isProgramNil() {
        assertTrue("isEmptyState failed to see when program is nil", Check.isEmptyState(Parse.pattern("( {} {} )")));
        assertFalse("isEmptyState erroneously reported program is nil when it's not", Check.isEmptyState(Parse.pattern("( 1 () )")));
    }

    @Test
    public void isStartNode() {
        Graph graph = new DefaultGraph("graph");
        graph.addNode("node");
        assertFalse("node is not a start rule", Check.isStartNode(graph.getNode("node")));
        graph.getNode("node").addAttribute(Attribute.START, true);
        assertTrue("node is a start rule", Check.isStartNode(graph.getNode("node")));
    }

    @Test
    public void containsDuplicateRules() {
        java.util.Set<Rule> rules = new HashSet<>();
        assertFalse("contains duplicate rules", Check.containsDuplicateRules(HContext.DEFAULT_CONTEXT, rules));
        rules.add(Parse.rule("(: () foo () )"));
        assertFalse("contains duplicate rules", Check.containsDuplicateRules(HContext.DEFAULT_CONTEXT, rules));
        rules.add(Parse.rule("(: () foo () )"));
        assertTrue("contains duplicate rules", Check.containsDuplicateRules(HContext.DEFAULT_CONTEXT, rules));
    }

    @Test
    public void containsAmbiguousRules() {
        java.util.Set<Rule> rules = new HashSet<>();
        assertFalse("zero rules can't be ambiguous", Check.containsAmbiguousRules(HContext.DEFAULT_CONTEXT, rules));
        rules.add(Parse.rule("(. (1 .Integer) -> () )"));
        assertFalse("only one rule can't be ambiguous", Check.containsAmbiguousRules(HContext.DEFAULT_CONTEXT, rules));
        rules.add(Parse.rule("(. (Integer .2) -> 1 )"));
        assertTrue("contains ambiguous rules", Check.containsAmbiguousRules(HContext.DEFAULT_CONTEXT, rules));
    }

    @Test
    public void isCompleteMatch() {
        java.util.Set<Rule> rules = new HashSet<>();
        Pattern state = Parse.pattern("({0 {1 abc}} ())");
        assertFalse("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));
        rules.add(Parse.rule("(. (1 ()) -> () )"));
        assertFalse("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));
        rules.add(Parse.rule("(. (0 ()) -> () )"));
        assertFalse("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));
        rules.add(Parse.rule("(. (abc ()) -> () )"));
        assertTrue("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));

        rules = new HashSet<>();
        state = Parse.pattern("(([List Any]) ())");
        assertFalse("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));
        rules.add(Rule.LIT_NIL_RULE);
        assertFalse("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));
        rules.add(Rule.LIT_PAIR_RULE);
        assertTrue("is complete match", Check.isCompleteMatch(HContext.DEFAULT_CONTEXT, state, rules));
    }

//    @Test
//    public void isWellFormed() {
//        Rule simpleWellFormed = Parse.rule("(: () foo foo )");
//        java.util.Set<Rule> rules = new HashSet<>();
//        //add the literal nil rule so this test works
//        rules.add(Rule.LIT_NIL_RULE);
//        //add simple well formed rule
//        rules.add(simpleWellFormed);
//        assertTrue("simple well formed", Check.isWellFormed(simpleWellFormed, rules));
//        Rule simpleIllFormed = Parse.rule("(: () foo () foo )");
//        rules = new HashSet<>();
//        rules.add(Rule.LIT_NIL_RULE);
//        rules.add(simpleIllFormed);
//        assertFalse("simple ill formed", Check.isWellFormed(simpleIllFormed, rules));
//    }

}
