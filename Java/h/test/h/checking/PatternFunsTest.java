package h.checking;

import h.model.HContext;
import org.junit.Test;
import h.pattern.Pattern;
import h.parsing.Parse;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class PatternFunsTest {

    @Test
    public void testIsLiteral() {
        String[] isLiteral = new String[] {
            "0", "a", "(0.a)"

        };
        for (String aLiteral : isLiteral) {
            assertTrue(aLiteral + " should be a literal", Parse.pattern(aLiteral).isLiteral());
        }
        String[] isNotLiteral = new String[] {
            "{}", "Integer", "Symbol", "Any", "(0.Any)", "[List Any]", "Boolean"
        };
        for (String notLiteral : isNotLiteral) {
            assertFalse(notLiteral + " should not be a literal", Parse.pattern(notLiteral).isLiteral());
        }
    }

    @Test
    public void testCardinality() {
        Object[] pairs = new Object[] {
                "{}", Pattern.Cardinality.ZERO,
                "0", Pattern.Cardinality.ONE,
                "a", Pattern.Cardinality.ONE,
                "(1.2)", Pattern.Cardinality.ONE,
                "(1.{1 {2 c}})", Pattern.Cardinality.FINITE,
                "{ { 1 2 } abc }", Pattern.Cardinality.FINITE,
                "{1 2}", Pattern.Cardinality.FINITE,
                "Any", Pattern.Cardinality.INFINITE,
                "Integer", Pattern.Cardinality.INFINITE,
                "Symbol", Pattern.Cardinality.INFINITE,
                "(1.Symbol)", Pattern.Cardinality.INFINITE,
                "(Symbol.a)", Pattern.Cardinality.INFINITE,
                "{Symbol {1 a}}", Pattern.Cardinality.INFINITE,
                "{1 {a Symbol}}", Pattern.Cardinality.INFINITE,
                "[List Any]", Pattern.Cardinality.INFINITE,
                "Boolean", Pattern.Cardinality.FINITE,
        };
        for (int i = 0; i < pairs.length; i += 2) {
            assertTrue(pairs[i] + " should have cardinality " + pairs[i + 1], Parse.pattern((String)pairs[i]).cardinality(HContext.DEFAULT_CONTEXT).equals(pairs[i + 1]));
        }
    }

    @Test
    public void testCountValues() {
        Object[] pairs = new Object[] {
                "0", 1,
                "a", 1,
                "(1.2)", 1,
                "(1.{1 {2 c}})", 3,
                "{ { 1 2 } abc }", 3,
                "{1 2}", 2,
                "Boolean", 2
        };
        for (int i = 0; i < pairs.length; i += 2) {
            Pattern pattern = Parse.pattern((String)pairs[i]);
            assertTrue(pattern + " should have value count " + pairs[i + 1], Parse.pattern((String)pairs[i]).countValues(HContext.DEFAULT_CONTEXT) == (int)pairs[i + 1]);
        }
    }

    @Test
    public void testIsInfinite() {
        String[] isInfinite = new String[] {
                "Integer", "Symbol", "Any", "(1.Any)", "{a Integer}", "[List Any]", "[List Integer]", "{Symbol Integer}"
        };
        for (String infinite : isInfinite) {
            assertTrue(infinite + " is infinite",
                    Parse.pattern(infinite).isInfinite(HContext.DEFAULT_CONTEXT));
        }
        String[] notInfinite = new String[] {
                "0", "a", "()", "(1.2)", "{1 {a {() (a.b)}}}", "Boolean"
        };
        for (String not : notInfinite) {
            assertFalse(not + " should not be a type",
                    Parse.pattern(not).isInfinite(HContext.DEFAULT_CONTEXT));
        }
    }

    @Test
    public void testIsMoreSpecific() {

        //
        String[] isMoreSpecific = new String[] {
                //literals are more specific than sets
                "0", "Any",
                "abc", "Any",
                "1", "Integer",
                "1", "Symbol",
                "a", "Symbol",
                "a", "Integer",
                "a", "{a b}",
                "a", "{1 2}",
                "a", "{Symbol Integer}",
                "true", "{true false}",
                "false", "{true false}",
                "bacon", "{true false}",
                "()", "Any",
                "(1.2)", "Any",
                "()", "Symbol",
                "(a.b)", "Symbol",
                "(1.(2.c))", "Symbol",
                "()", "Integer",
                "(a.b)", "Integer",
                "(1.2)", "Integer",
                "()", "{a b}",
                "(a.b)", "{1 2}",
                "{1 2}", "{1 {2 3}}",
                "{4 5}", "{{1 2} 3}",
                "(b.2)", "{Symbol Integer}",
                "(1.(2.(3.())))", "(1.(2.Any))",
                "(1.())", "({1 2}.())",
                "()", "(Any .Any)",
                "(cast.t:Any)", "(Any .Any)",
                "(() .s:Any)", "((a:Any .()) .s:Any)",
                "((a:Any .()) .s:Any)", "((a:Any .b:Any) .s:Any)",
                "[List 5]", "[List Integer]",
                "[List Integer]", "[List Any]",
                "[List Integer]", "Any",
                "{Symbol Integer}", "Any",
        };
        for (int i = 0; i < isMoreSpecific.length; i+=2) {
            Pattern lhs = Parse.pattern(isMoreSpecific[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern rhs = Parse.pattern(isMoreSpecific[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertTrue (lhs + " should be more specific than " + rhs, PatternFuns.isMoreSpecific(HContext.DEFAULT_CONTEXT, lhs, rhs));
        }
        String[] notMoreSpecific = new String[] {
                "Any", "0",
                "Any", "a",
                "Any", "()",
                "Any", "(Any.Any)",
                "Integer", "(0.1)",
                "Integer", "(b.a)",
                "Symbol", "0",
                "Symbol", "(0.1)",
                "Symbol", "(b.a)",
                "{true false}", "bacon",
                "{true false}", "0",
                "{1 {2 3}}", "4",
                "{a b}", "a",
                "{a {b c}}", "{a b}",
                "{1 {2 3}}", "{a b}",
                "{1 2}", "2",
                "{1 {2 3}}", "{2 3}",
                "{1 2}", "{1 2}",
                "{1 Any}", "{1 2}",
                "{a {b 2}}", "c",
                "{a {b 2}}", "()",
                "{() 2}", "()",
                "(1.(2.(3.())))", "(1.(2.()))",
                "(1.(2.()))", "(1.(2.(3.())))",
                "({1 2}.())", "(1.())",
                "[List Integer]", "[List 5]",
                "[List Any]", "[List Integer]",
                "Any", "{Symbol Integer}",
        };
        for (int i = 0; i < notMoreSpecific.length; i+=2) {
            Pattern lhs = Parse.pattern(notMoreSpecific[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern rhs = Parse.pattern(notMoreSpecific[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertFalse(lhs + " should not be more specific than " + rhs, PatternFuns.isMoreSpecific(HContext.DEFAULT_CONTEXT, lhs, rhs));
        }
    }

    @Test
    public void testIsSubset() {

        //lhs is a subset of rhs if all the values of the
        //lhs set are contained in the set of values of rhs
        String[] isSubset = new String[] {
                //everything is a subset of any
                "Any", "Any",
                "0", "Any",
                "abc", "Any",
                "()", "Any",
                "(1.2)", "Any",
                "(Integer .Symbol)", "Any",

                //literals of a type are a subset of that type
                "0", "Integer",
                "1", "Integer",
                "a", "Symbol",

                //a set of literals of a type is a subset of that type
                "{1 2}", "Integer",
                "{a b}", "Symbol",

                //a member of a set is a subset of the set
                "a", "{a b}",
                "a", "{Symbol Integer}",
                "true", "{true false}",
                "false", "{true false}",
                "(b.2)", "{Symbol Integer (Symbol .Integer)}",

                //a typename is a subset of a set of typenames
                "Integer", "{Integer Symbol}",
                "Symbol", "{Integer Symbol}",

                //a subset of a set is, you guessed it, a subset of that set
                "{1 2}", "{1 2 3}",
                "{1 2}", "{1 2 3}",

                //misc
                "(1.(2.(3.())))", "(1.(2.Any))",
                "(1.())", "({1 2}.())",

                //type constructor
                "()", "[List Any]",
                "()", "[List Integer]",
                "(1 .(2 .()))", "[List Any]",
                "(two .(2 .()))", "[List Any]",
                "(two .(three .()))", "[List Any]",
                "(1 .(2 .()))", "[List Integer]",
                "(1 .(2 .()))", "[List {1 2}]",
                "(1 .(Integer .()))", "[List Integer]",
                "[List 5]", "[List Integer]",
                "[List {1 two}]", "[List {Integer Symbol}]",
                "true", "Boolean",
                "false", "Boolean",
        };
        for (int i = 0; i < isSubset.length; i+=2) {
            Pattern lhs = Parse.pattern(isSubset[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern rhs = Parse.pattern(isSubset[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertTrue(lhs + " should be a subset of " + rhs, PatternFuns.isSubset(HContext.DEFAULT_CONTEXT, lhs, rhs));
        }
        String[] notSubset = new String[] {

                //any should not be a subset of integer
                "Any", "Integer",
                "Any", "Symbol",

                //literals of a type are a subset of that type
                "a", "Integer",
                "1", "Symbol",

                //a set of literals of a type is a subset of that type
                "{a b}", "Integer",
                "{1 2}", "Symbol",

                //a member of a set is a subset of the set
                "c", "{a b}",
                "()", "{Symbol Integer}",
                "bacon", "{true false}",
                "eggs", "{true false}",
                "(1.two)", "{Symbol {Integer (Symbol .Integer)}}",
                "(1.two)", "{{Symbol Integer} (Symbol .Integer)}",

                //a typename is a subset of a set of typenames
                "Any", "{Integer Symbol}",
                "Any", "{Integer Symbol}",

                //a subset of a set is, you guessed it, a subset of that set
                "{1 4}", "{1 {2 3}}",
                "{1 4}", "{{1 2} 3}",

                //misc
                "(1.(3.(2.())))", "(1.(2.Any))",
                "(3.())", "({1 2}.())",

                //
                "{1 ()}", "{Symbol Integer}",
                "{() two}", "{Symbol Integer}",

                //type constructor
                "{}", "[List Any]",
                "(foo)", "[List Integer]",
                "(1 .(2 .3))", "[List Any]",
                "(two .(2 .four))", "[List Any]",
                "(two .(three .3))", "[List Any]",
                "(1 .(two .()))", "[List Integer]",
                "(one .(2 .()))", "[List {1 2}]",
                "(1 .(Symbol .()))", "[List Integer]",
                "[List five]", "[List Integer]",
                "[List {1 ()}]", "[List {Integer Symbol}]",
                "foo", "Boolean",
                "0", "Boolean",
        };
        for (int i = 0; i < notSubset.length; i+=2) {
            Pattern lhs = Parse.pattern(notSubset[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern rhs = Parse.pattern(notSubset[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertFalse(lhs + " should not be a subset of " + rhs, PatternFuns.isSubset(HContext.DEFAULT_CONTEXT, lhs, rhs));
        }
    }

    @Test
    public void testStrictMatch() {

        String[] pairs = new String[] {
            "1", "1",
            "{1 2}", "1",
            "two", "two",
            "{two three}", "two",
            "Integer", "1",
            "Symbol", "two",
            "{Integer Symbol}", "two",
            "Any", "{1 two}",
            "(Integer .Symbol)", "(1 .two)",
            "Boolean", "{true false}",
            "Boolean", "true",
            "Boolean", "false",
            "[List Any]", "()",
            "[List Any]", "(1 two (three))",
            "[List Boolean]", "(true false {true false} Boolean)",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern pattern = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern object = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertTrue(pattern + " should strictly match " + object, PatternFuns.strictMatch(HContext.DEFAULT_CONTEXT, pattern, object));
        }

        pairs = new String[] {
                "1", "one",
                "1", "{ 1 2 }",
                "two", "2",
                "two", "{two three}",
                "two", "three",
                "{1 two}", "Any",
                "(Integer .Symbol)", "(1 .())",
                "[List Any]", "(1 two (three) .four)",
                "[List Boolean]", "(true false {true false} foo)",
                "[List Boolean]", "(true false {true false} Any)",
                "[List Boolean]", "(true false {true false} .true)",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern pattern = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern object = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertFalse(pattern + " should not strictly match " + object, PatternFuns.strictMatch(HContext.DEFAULT_CONTEXT, pattern, object));
        }
    }

    @Test
    public void testPossibleMatch() {

        String[] pairs = new String[] {
                "1", "1",
                "1", "{1 2}",
                "{1 2}", "1",
                "{1 2}", "2",
                "{1 2}", "{1 2 3}",
                "{1 2}", "Integer",
                "two", "two",
                "two", "Symbol",
                "two", "Any",
                "{two three}", "two",
                "Integer", "1",
                "Symbol", "two",
                "{Integer Symbol}", "two",
                "Any", "{1 two}",
                "(Integer .Symbol)", "(1 .two)",
                "Boolean", "{true false}",
                "Boolean", "true",
                "Boolean", "false",
                "[List Any]", "()",
                "[List Any]", "(1 two (three))",
                "[List Boolean]", "(true false {true false} Boolean)",
                "()", "[List Boolean]",
                "Symbol", "Any",
                "Integer", "Any",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern pattern = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern object = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertTrue(pattern + " should possibly match " + object, PatternFuns.possibleMatch(HContext.DEFAULT_CONTEXT, pattern, object));
        }

        pairs = new String[] {
                "1", "one",
                "1", "{ 1 2 }",
                "two", "2",
                "two", "{two three}",
                "two", "three",
                "{1 two}", "Any",
                "(Integer .Symbol)", "(1 .())",
                "[List Any]", "(1 two (three) .four)",
                "[List Boolean]", "(true false {true false} foo)",
                "Boolean", "[List Boolean]",
                "[List Integer]", "[List Boolean]",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern pattern = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern object = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertFalse(pattern + " should not possibly match " + object, PatternFuns.strictMatch(HContext.DEFAULT_CONTEXT, pattern, object));
        }
    }

    @Test
    public void testOrthogonal() {

        String[] pairs = new String[] {
            "a", "1",
            "1", "a",
            "{1 2}", "{3 4}",
            "{1 {2 3}}", "{4 {5 6}}",
            "{1 2 3}", "{4 {5 6}}",
            "{1 {2 3}}", "{4 5 6}",
            "{{1 2} 3}", "{Symbol ()}",
            "(abc)", "(def)",
            "(Integer)", "(Symbol)",
            "(Symbol)", "(Integer)",
            "(().Symbol)", "(().Integer)",
            "( ( out . p:Any ) ( o:Any . s:Any ) e:Any )", "( ( two_states . p:Any ) s:Any e:any )",
            "Boolean", "[List Boolean]",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern lhs = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern rhs = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertTrue(lhs + " should be orthogonal to " + rhs, PatternFuns.orthogonal(HContext.DEFAULT_CONTEXT, lhs, rhs));
        }

        pairs = new String[] {
            "1", "1",
            "1", "Integer",
            "Integer", "1",
            "a", "a",
            "a", "Symbol",
            "Symbol", "a",
            "{1 2}", "{2 3}",
            "{1 {a 3}}", "{Symbol ()}",
            "(abc)", "(abc)",
            "1", "Integer",
            "Integer", "1",
            "(Integer)", "(Integer)",
            "(Symbol)", "(Symbol)",
            "(().Symbol)", "(().Symbol)",
            "Any", "(().Symbol)",
            "a:Any", "b:Any",
            "[List {true false}]", "[List Boolean]",
            "[List Boolean]", "[List Boolean]",
            "[List Symbol]", "[List Integer]", //should not be orthogonal because both match ()
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern lhs = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            Pattern rhs = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT).unwrapBindingPattern();
            assertFalse(lhs + " should not be orthogonal to " + rhs, PatternFuns.orthogonal(HContext.DEFAULT_CONTEXT, lhs, rhs));
        }
    }
}
