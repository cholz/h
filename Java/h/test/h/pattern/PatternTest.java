package h.pattern;

import h.model.HContext;
import h.model.UserDefinedType;
import org.junit.Test;
import h.parsing.Parse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class PatternTest {

    @Test
    public void testEquals() {
        String[] pairs = new String[] {
            "0",                    "0",
            "1",                    "1",
            "1000000000",           "1000000000",
            "a",                    "a",
            "bb",                   "bb",
            "aaaaaaaaaaa",          "aaaaaaaaaaa",
            "()",                   "()",
            "(().())",              "(().())",
            "(().())",              "(())",
            "(a.b)",                "(a.b)",
            "(a.(().()))",          "(a.(().()))",
            "(a.(().()))",          "(a.(()))",
            "{a {b c}}",              "{a {b c}}",
            "{}",                   "{}",
            "[Boolean]",               "Boolean",
            "[List Any]",           "[ List Any ]",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            assertTrue ( pairs[i] + " and " + pairs[i + 1] + " should be equal",
                Parse.pattern(pairs[i]).equals(Parse.pattern(pairs[i + 1])));
        }

        pairs = new String[] {
            "0",                    "1",
            "1",                    "0",
            "1000000000",           "011111111",
            "a",                    "b",
            "bb",                   "aa",
            "aaaaaaaaaaa",          "bbbbbbbbbb",
            "()",                   "(1.2)",
            "(().())",              "(() ())",
            "(().())",              "(() () .())",
            "(a.b)",                "(a.c)",
            "(a.(().()))",          "(a.(() ()))",
            "(a.(().()))",          "(a.(b))",
            "{a {b c}}",              "{a {z c}}",
            "{c {z a}}",              "{a {b c}}",
            "{}",                   "Any",
            "{}",                   "()",
            "{}",                   "{a {b c}}",
            "[Boolean]",               "Fool",
            "[List Any]",           "[ List Integer ]",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            assertFalse(pairs[i] + " and " + pairs[i + 1] + " should not be equal",
                Parse.pattern(pairs[i]).equals(Parse.pattern(pairs[i + 1])));
        }
    }

    @Test
    public void testHashCode() {

        String[] pairs = new String[] {
            "0", "0",
            "1", "1",
            "1000000000", "1000000000",
            "a:{a b}", "a:{a b}",
            "a:{0 1}", "a:{a b}",
            "{a b}", "{a b}",
            "{0 1}", "{0 1}",
            "a", "a",
            "aaaaaaaaaaa", "aaaaaaaaaaa",
            "()", "()",
            "(().())", "(().())",
            "(a.b)", "(a.b)",
            "(a.(().()))", "(a.(().()))",
            "0", "1",
            "1", "0",
            "1000000000", "0111111111",
            "a", "b",
            "aaaaaaaaaaa", "bbbbbbbbbbb",
            "()", "( a . b )",
            "(().())", "()",
            "(a.b)", "(x.y)",
            "(a.(().()))", "(z.())",
            "{a {b c}}", "{a {b c}}",
            "{1 2}", "{1 2}",
            "{a b}", "{a {b c}}",
            "{1 2}", "{2 3}",
            "{}",                   "{}",
            "{}",                   "Any",
            "{}",                   "()",
            "{}",                   "{a {b c}}",
            "[Boolean]",               "Boolean",
            "[List Any]",           "[ List Any ]",
            "[Boolean]",               "Fool",
            "[List Any]",           "[ List Integer ]",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            if (Parse.pattern(pairs[i]).equals(Parse.pattern(pairs[i + 1]))) {
                assertEquals ( pairs[i] + " and " + pairs[i + 1] + " should be equal",
                    Parse.pattern(pairs[i]).hashCode(), Parse.pattern(pairs[i + 1]).hashCode());
            }
        }
    }

    @Test
    public void testToStringWithDepth() {
        assertTrue("toString with depth", new Pair(Nil.VALUE, Nil.VALUE).toString(3).equals("( () )"));
        assertTrue("toString with depth", new Pair(Nil.VALUE, Nil.VALUE).toString(4).equals("(...)"));
    }

    @Test
    public void testTypeConstructor() {

        TypeConstructor tc = (TypeConstructor) Parse.pattern("[List Integer]");
        Pattern construction = tc.construct(HContext.DEFAULT_CONTEXT);
        assertTrue("[List Integer] should construct {() (Integer .[List Integer])} but constructed " + construction,
                construction.equals(Parse.pattern("{() (Integer .[List Integer])}")));

        tc = (TypeConstructor) Parse.pattern("Boolean");
        construction = tc.construct(HContext.DEFAULT_CONTEXT);
        assertTrue("Boolean should construct {true false} but constructed " + construction,
                construction.equals(Parse.pattern("{true false}")));

    }

    @Test
    public void testContains() {

        TypeConstructor tc = (TypeConstructor) Parse.pattern("[List Integer]");
        Pattern construction = tc.construct(HContext.DEFAULT_CONTEXT);
        assertTrue(construction.toString() + " should contain " + tc.toString(), construction.contains(tc));

        tc = (TypeConstructor) Parse.pattern("Boolean");
        construction = tc.construct(HContext.DEFAULT_CONTEXT);
        assertFalse(construction.toString() + " should not contain " + tc.toString(), construction.contains(tc));
    }

    @Test
    public void testIsRecursive() {

        TypeConstructor tc = (TypeConstructor) Parse.pattern("[List Integer]");
        assertTrue(tc.toString() + " should be recursive ", tc.isRecursive(HContext.DEFAULT_CONTEXT));

        tc = (TypeConstructor) Parse.pattern("Boolean");
        assertFalse(tc.toString() + " should not be recursive ", tc.isRecursive(HContext.DEFAULT_CONTEXT));
    }

    @Test
    public void testNormalize() {
        String[] pairs = new String[] {
            "{1 2}",                            "{1 2}",
            "{a {b c}}",                        "{a b c}",
            "{{a b} c}",                        "{a b c}",
            "{Any 1 2 3}",                      "Any",
            "{Symbol 1 two}",                   "{Symbol 1}",
            "{Symbol 1 two Integer}",           "{Symbol Integer}",
            "{Symbol 1 {two Integer}}",         "{Symbol Integer}",
            "{{Symbol 1 two} Integer}",         "{Symbol Integer}",
            "1",                                "1",
            "two",                              "two",
            "(1 .{1 2})",                       "(1 .{ 1 2 })",
            "(1 .{Symbol 1 two})",              "(1 .{Symbol 1})",
            "({Any 1 2 3} .{Symbol 1 two})",    "(Any .{Symbol 1})",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern lhs = Parse.pattern(pairs[i]).normalize(HContext.DEFAULT_CONTEXT);
            Pattern rhs = Parse.pattern(pairs[i + 1]).normalize(HContext.DEFAULT_CONTEXT);
            assertTrue(lhs + " and " + rhs + " should be equal ", lhs.equals(rhs));
        }
    }

    @Test
    public void testExpand() {
        String[] pairs = new String[] {
            "1", "1",
            "(1 .2)", "(1 .2)",
            "({1 2} .2)", "{(1 .2) (2 .2)}",
            "(1 .{1 2})", "{(1 .1) (1 .2)}",
            "({1 2} .{3 4})", "{(1 .3) (1 .4) (2 .3) (2 .4)}",
        };
        for (int i = 0; i < pairs.length; i+=2) {
            Pattern lhs = Parse.pattern(pairs[i]).expand(HContext.DEFAULT_CONTEXT);
            Pattern rhs = Parse.pattern(pairs[i + 1]);
            assertTrue(lhs + " and " + rhs + " should be equal ", lhs.equals(rhs));
        }
    }

}
