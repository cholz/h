

grammar h;

h
    : h_rule* EOF
    ;

body
    : expr*
    ;

args
    : pattern*
    ;

h_rule
    : '(' '.' before=pattern '->' after=pattern ')'                     #basicRule
    | '(' '=' '(' before=args ')' name=pattern '(' after=args ')' ')'   #stackRule
    | '(' ':' '(' args ')' name=pattern body ')'                        #functionRule
    | '(' '>' body ')'                                                  #entryPointRule
    | '(' '[' TYPENAME SYMBOL* ']' pattern ')'                          #userDefinedTypeRule    //isn't really a rule
    ;

pattern
    : NATURAL                               #naturalPattern
    | SYMBOL                                #symbolPattern
    | SYMBOL ':' pattern                    #bindingPattern
    | TYPENAME                              #typenamePattern
    | '{' pattern* '}'                      #optionPattern
    | '[' TYPENAME pattern* ']'             #typeConstructorPattern
    | '(' pattern* ')'                      #listPattern        //either nil or a nil terminated list of nested pairs
    | '(' pattern+ '.' last=pattern ')'     #pairPattern        //a list or pair with possibly non nil termination
    ;

expr
    : NATURAL                             #naturalExpr
    | SYMBOL                              #symbolExpr
    | '(' expr* ')'                       #listExpr
    | '(' expr+ '.' last=expr ')'         #pairExpr
    ;

SYMBOL 
    : ('a'..'z' | '_' ) ('a'..'z' | 'A'..'Z' | '0'..'9' | '_' )*
    ;

TYPENAME 
    : ('A'..'Z' ) ('a'..'z' | 'A'..'Z' | '0'..'9' | '_' )*
    ;

NATURAL : '0'..'9'+ ;

//ignore

WHITESPACE : (' ' | '\n' | '\t' | '\r') + -> skip ;

COMMENT : '//' ~('\n'|'\r')* '\r'? '\n' -> skip ;

