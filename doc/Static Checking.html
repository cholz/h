<h2>Static Checking</h2>
<br/>

The last few posts on H have are meant to provide you with enough understanding of the way H works to understand this post which is the real goal of this project. I talked in the introduction about languages that inspired me to develop H. Those languages, UserRPL, Joy, Forth, and Factor to name a few, all suffer from the same problem. That is, they don't attempt to check the correctness of programs before they run. Correctness is left for the programmer to determine through testing and luck. I intended for this project to produce a language with similar characteristics to those languages (stack-oriented, concatenative, homoiconic) with the added benefit of static type (correctness) checking. It has been partially successful.
<br/>
<br/>

When I talk about correctness I mean correct in the sense that a program is free from type errors. Of course, any non-trivial program could encounter exceptional circumstances at runtime, but I believe that a language with sufficiently powerful static checking can, at least, determine that those exceptional circumstances are handled by deliberate code rather than left to cause a unhandled runtime exception. H language is designed so that 

<h3>Rule Uniqueness and Non-ambiguity</h3>
Some simple and necessary things to check in an H program are the uniqueness and non-ambiguity of rules. As I have mentioned in previous posts an H program consists only of rules that transform H machine state. If the rules are not unique, or are ambiguous, then assumptions made in later checking will not hold. Fortunately it is a simple matter to check these things. 
<br/>
<br/>
Before I talk about what it means for rules to be unique or ambiguous I have to talk about a concept called specificity. Each rule in H contains a pattern against which the H machine state is compared. When a rule's pattern matches the current state that rule is fired and its effect take place. Sometimes though more than one rule can match a given state. In that case which rule should fire? The answer is that the most specific rule is the one that should fire. Let me give an example:
<pre>    (: (Integer) foo (integer) out )
    (: (Any) foo (not integer) out )</pre>
These two rules would both match the state <code>((foo) (42))</code> since each looks for the symbol <code>foo</code> on the program stack and the patterns <code>Integer</code> and <code>Any</code> both match the value <code>42</code>. The specificity of each rule's before pattern is used to determine which one should actually match and fire when presented with a particular value. The two rules above when written in basic form look like:
<pre>    (. ( (foo .p:Any) (Integer .s:Any) ) -> (...) )
    (. ( (foo .p:Any) (Any .s:Any) ) -> (...) )</pre>
I left the after pattern empty because it is not relevant in determining rule specificity. The program part of each rule is the same <code>(foo .p:Any)</code> and cannot be used to determine specificity. The data stack part of each pattern differs in the type of element on the top of the data stack. The first pattern matches only integer values while the second matches any value including integers. Since the first pattern matches a subset of the values of the second it can be said that the first is more specific than the second. This method is used to determine which rule of a set of matching rules will fire. I believe this is a method provides a benefit when compared to pattern matching systems in other languages because it doesn't depend on any ordering of the text of the source program. The sequence
<pre>    (: (Integer) foo (integer) out )
    (: (Any) foo (not integer) out )</pre>
is exactly equivalent to the sequence
<pre>    (: (Any) foo (not integer) out )
    (: (Integer) foo (integer) out )</pre>
because it can be determined that one rule has a higher specificity than the other.
<br/>
<br/>
Back to uniqueness and ambiguity. Two rules are not unique if they match the same state and neither is more specific than the other. In the example above the rules are considered unique because although in some cases they do match the same value they can also be ordered by specificity and so there is no ambiguity about which rule should fire. An example of rules that are not unique is:
<pre>    (: ({0 1}) foo (zero one) out )
    (: ({1 2}) foo (one two) out )</pre>
In this case each rule may match the same state, e.g. <code>( (foo) (1) )</code>, but neither can be said to be more specific than the other. This is an error because the rules are not unique.
<br/>
<br/>
Ambiguous rules are similar to non unique rules. A pair of ambiguous rules both may match the same state, but rather than neither being more specific than the other both are more specific than the other. This is a side effect of trying to determine specificity for a pattern that contains a pair. When determining the specificity of two pair patterns each corresponding element of the pairs is examined for specificity and if either is more specific than the other the pair with the more specific element is said to be more specific. Let me illustrate with examples. In the following list I will use the symbol <code>&lt;</code> to indicate the pattern on the left hand side is more specific than that on the right hand side:
<pre>    (1.two) &lt; (Integer.Symbol)    //each element of lhs is a subset of the corresponding element in rhs
    (1.two) &lt; (Integer.two)    //at least one element of lhs is a subset of the corresponding element in rhs
    (1.Symbol) &lt; (Integer.two)    //at least one element of lhs is a subset of the corresponding element in rhs
    (Integer.two) &lt; (1.Symbol)    //at least one element of lhs is a subset of the corresponding element in rhs</pre>
The first two cases in that list should be straight forward. The last two illustrate the case of ambiguous patterns. Each of the patterns in the last two examples is more specific than the other. This could be avoided by ordering the specificity comparison of elements of pairs from left to right, but that has other problems. By saying the last two are more specific than the other forces the programmer to write unambiguous rules that don't depend on any arbitrary ordering. An example of two rules that are ambiguous:
<pre>    (: (1 Symbol) foo (1 symbol) out )
    (: (Integer two) foo (symbol two) out )</pre>
These are just rules fabricated using the idea presented in the ambiguous pattern example above. This would be an error in an H program because it's impossible to order these rules by specificity when they both match the example state <code>( (foo) (two 1) )</code>.
<br/>
<br/>
In order to check uniqueness and ambiguity first the entire program is read in. The complete set of rules is then traversed by a pair of nested loops. Each rule is checked against each other rule to determine if they can match the same state and their relative specificity. Uniqueness and ambiguity can be determined with the information gathered that way.

<h3>Completeness</h3>

Beside uniqueness and ambiguity of rules, the H checker checks that, for each possible state of the H machine during execution of an H program, there is always a matching rule or set of rules that completely covers all possible values for that state. The current H checker is able to do this at least partially. There are some programs that are correct, but either fail checking or cause the checker to run indefinitely. I'm not sure if it is possible to check this property completely without adding more limitations to the language. In order to check for completeness the H checker has to interpret the given program in an abstract sense. In the case of the hello world program it is not a problem to simply cover all possible paths of execution which is more of a concrete interpretation than an abstract one. Sometimes the checker doesn't have concrete information about the value of the state and it must use abstract values which are sets of possible concrete values. An example of this is given by the following program:
<pre>    (: (Integer) foo (got int) out )
    (> in foo )</pre>
When the checker attempts to check this program it begins abstract interpretation with the entry point rule. That rule replaces the empty program with the initial one given by <code>(in foo end)</code>. In that program the first rule to match is the <code>in</code> rule. The in rule receives one value from the console and places it onto the data stack. The interesting thing about the <code>in</code> rule is that the value that it receives could be any possible value. The checker has no way to know what value will be received because it is doing its checking before runtime. Therefore, it uses the signature of the <code>in</code> rule when reconstructing the next state. The state that the checker would construct after firing the <code>in</code> rule is given by <code>( (foo end) (Any) )</code>. The <code>in</code> rule says that it places a value with any type onto the data stack. At runtime that is not possible because the <code>in</code> rule only accepts expressions from the console and expressions don't include typename patterns. This is what I mean when I say the H checker has to interpret the program in an abstract sense. At runtime only one concrete value would be places on the data stack by the <code>in</code> rule, but the checker can't assume any concrete value and must use the abstract value <code>Any</code>. In the example program above the checker would report an error when attempting to match the state <code>( (foo end) (Any) )</code>. This is because there is no complete match. The rule <code>(: (Integer) foo (got int) out )</code> would match in the case that the concrete value provided by <code>in</code> is an integer, but there is no possible match when the concrete value is a pair or symbol. In order for the example program given above to pass checking it would have to be augmented as follows:
<pre>    (: (Integer) foo (got int) out )
    (: (Any) foo (got other than int) out )
    (> in foo )</pre>
In cases above the state <code>( (foo end) (Any) )</code> is completely matched and the checker would be made happy.
<br/>
<br/>
The program example directly above is an example of branching in H. When the checker encounters the state <code>( (foo end) (Any) )</code> it must then examine both paths produced by the alternate rules that match that state. Down each path the checker verifies that each state has a complete match. In addition to branching the H language includes loops in the form of recursion. In order for the checker to successfully check recursive programs it must be able to recognize loops and stop processing them. This is currently a major limitation on the types of programs that can be checked. The method that the checker uses to find loops requires that the program stack and data stack are the same size. This precludes the programmer from writing recursive programs in the most natural way. As an example here is a naive program to compute the factorial of a number taken from the console:
<pre>    (: ({0 1})        fact 1)
    (: (n:Integer)    fact n n 1 sub fact mul )
    (: (Any)          fact (error not an integer) )
    (> in fact out )</pre>
This program assumes the existence of <code>sub</code> and <code>mul</code> rules that each do the expected operations on the top two items of the data stack. Although the program is correct, the H checker can't currently determine so because it is unable to detect the loop. It simply recursively attempts to check the loop endlessly. In order for the checker to succeed with the factorial program the programmer must use an accumulator value kept on the data stack as so:
<pre>    (: (n:Integer)    fact 1 n factImpl )
    (: (Any)          fact (error not an integer) )
    (: (a:Integer {0 1})        factImpl a )
    (: (a:Integer n:Integer)    factImpl a n mul n 1 sub factImpl )
    (> in fact out )</pre>
In this version of the factorial program the <code>factImpl</code> rule is written in such a way that at each invocation the stack and program size is static. In this way the H checker can detect the loop and properly deduce that this program is correct.
<br/>
<br/>
The H language at this point is far from complete. I hope this series of posts has given you an idea of what it is meant to do and what it is capable of at this point.





