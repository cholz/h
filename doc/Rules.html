
<h2>Rules</h2>
<br/>

In the last post I talked about the syntax of expressions and patterns in H. With that understanding we can talk about rules. An H program is made up of only rules. Like other languages' main function there must be one rule defined as a program entry point. There is a built in rule that halts execution like returning from main. Other rules act like H's functions. User defined rules in H are pure, i.e. they don't have side effects. They only transform the state of the H machine. However, part of that state includes the program that has yet to be evaluated. A user defined rule is allowed to prepend symbols to the as-yet-evaluated program that will result in the firing of rules that do have side effects. As I talked about in an earlier post the H machine consists of state and a set of rules. Pattern matching is used to determine which rule of the set should fire. Each rule has a before pattern that is used in this determination and to deconstruct the state. Each rule has also an after pattern that is used to reconstruct the state if the rule fired.
<br/>
<br/>

The syntax of basic rules is the following:
<pre>    rule
        : '(' '.' pattern '->' pattern ')'
        ;</pre>
The basic rule has its before pattern on the left hand side of <code>-></code> and its after pattern on the right hand side. An example of an entry point rule using this syntax looks like:
<pre>    (. ( () () ) -> ( ((hello world) out end) () ) )</pre>
That's a lot of parenthesis and is rather ugly. There are some sugared forms of rules that I'll get to in a little bit that make writing rules easier. This is an entry point rule because it matches the empty state <code>( () () )</code>. There must be exactly one such rule in an H program. This rule replaces the empty program <code>()</code> with a hello world program <code>( (hello world) out end )</code>. It doesn't modify the data stack because it is the same <code>()</code> on both sides of the rule. If this rule were to fire it would transform the state to look like <code>( ((hello world) out end) () )</code>. Then the H machine would look for another matching rule. The rule that would match is the built in literal pair rule:
<pre>    (. ( (pair:(Any.Any) .p:Any) s:Any ) -> ( p (pair .s) ) )</pre>
This rule matches when there is a pair as the first item of the program stack. It transfers the pair to the top of the data stack. This rule makes use of the binding patterns I talked about in the last post. The symbol <code>pair</code> is bound to the value on the top of the program stack that matches the pattern <code>(Any.Any)</code>. Similarly the symbol <code>p</code> is bound to the rest of the program stack, and the symbol <code>s</code> is bound to the entirety of the data stack. The right hand side of the rule makes use of those bindings to rebuild the state. The rest of the program (bound to <code>p</code>) is put in the program place of the state, and a new pair <code>(pair .s)</code> is put in place of the data stack. The pattern <code>(pair .s)</code> makes use of two bindings from the left hand side of the rule and has the effect of pushing the literal pair onto the data stack. There are similar built in rules for literal integers, and the empty list <code>()</code>. After the literal pair rule has fired the H machine state would look like <code>( (out end) ((hello world)) )</code>. The next rule that would match is the <code>out</code> rule:
<pre>    (. ( (out .p:Any) (Any .s:Any) ) ~> ( p s ) )</pre>
This rule uses the symbol <code>~></code> rather than <code>-></code> to indicate that it has side effects and it can't be defined in H, but rather must be implemented by the compiler or interpreter. The rule does show its effect on the H machine state. It matches the literal symbol <code>out</code> on top of the program stack and any value on top of the data stack. The value on the data stack gets printed to the console as a side effect of this rule firing. After the out rule fires the state would look like <code>( (end) () )</code>. The next rule to match is the <code>end</code> rule:
<pre>    (. ( (end .Any) Any ) -> ( {} {} ) )</pre>
This rule doesn't care about binding anything because it transforms the state into another kind of empty state <code>( {} {} )</code> where the program and stack are empty options. No pattern matches this state and therefore execution halts.
<br/>
<br/>
Those rules all used the basic form to show how it works, but there are simpler forms that make rule writing less tedious. Any rule in simple form can be written in the basic form. The first simple form is the entry point rule syntax:
<pre>    entryPointRule
        : '(' '>' expr* ')'
        ; </pre>
Using the simpler form of the entry point rule the example above could be written as <code>(> (hello world) out )</code>.
<br/>
<br/>
Another simple rule form is the data stack transformation with this syntax:
<pre>    stackRule
        : '(' '=' '(' before=pattern* ')' name=pattern '(' after=pattern* ')' ')'
        ; </pre>
This rule has a pattern, called name, nested between two representations of the data stack. The left hand side is what the stack should look like before the rule fires and the right hand side is what the stack should look like after the rule fires. A rule written in this form such as the rule that duplicates the top item on the stack <code>(= (v:Any) dup (v v) )</code> would be written in basic form as <code>(. ( (dup .$0:Any) (v:Any .$1:Any) ) -> ( $0 (v v .$1) ) )</code> where the symbols beginning with <code>$</code> are generated by the implementation so as to avoid variable capture.
<br/>
<br/>
The final simple rule form is the function rule:
<pre>    functionRule
        : '(' ':' '(' pattern* ')' pattern expr* ')'
        ;</pre>
This rule is used to simplify the writing of rule that simply prepends expressions to the program stack. For example a rule that increments an integer by two could be written as <code>(: (i:Integer) inc2 i inc inc )</code> and could also be written in basic form as <code>(. ((inc2 .p:Any) (i:Integer .s:Any)) -> ((i inc inc .p) s) )</code>. 
<br/>
<br/>
An important point about the data stack and function rules is the order of items appearing in the data stack representations. The order of items in the data stack representation is reversed in the simple forms to make rule writing more natural. In the rule <code>(= (second top) foo () )</code> the stack pattern would be re-written as <code>(top second .$0:Any)</code> because of the way that the data and program stacks are implemented as linked lists. In the entry point rule <code>(> 1 2 )</code> the integers <code>1</code> and <code>2</code> would be pushed onto the stack in the order they are listed and so the stack would end up looking like </code>(2 .(1 .()))</code> before the <code>end</code> rule fires. If I wanted a rule to match that stack state I could write the pattern as <code>(2 .(1 .s:Any))</code> in a basic form rule, or as <code>(1 2)</code> in a stack or function rule.
<br/>
<br/>
The complete list of built in rules:
<pre>    //IO rules 
    //get a value from the console
    (= () in (Any) )
    //print a value to the console
    (= (v:Any) out () )
    
    //stack manipulation
    //remove the item on the top of the data stack
    (= (Any) drop () )
    //duplicate the item on the top of the data stack
    (= (v:Any) dup (v v) )
    //swap two items on the data stack
    (= (a:Any b:Any) swap (b a) )
    
    //pair operations
    //create a pair from two items on the data stack
    (= (a:Any b:Any) pair ((a .b)) )
    //get the first item in a pair
    (= ((fst:Any .Any)) fst (fst) )
    //get the second item in a pair
    (= ((Any .snd:Any)) snd (snd) )
    
    //the i rules allow the use of lists as code, 
    //e.g. (> (42 inc) i) has the same effect as (> 42 inc)
    //the idea of this rule comes from the i word in the Joy language
    //i on the empty list has no effect
    (: (()) i )
    //i on a list with one item evaluates that item
    (: ((a:Any)) i a )
    //the general case, evaluate first item and recurse on rest of list
    (: ((f:Any .r:[List Any])) i f r i )

    //minimal arithmetic operations
    //actual operation implemented by compiler or interpreter
    (= (i:Integer) inc (Integer) )
    (= (i:Integer) dec (Integer) )

    //literal rules
    //literal integer
    (= () n:Integer (n) )
    //literal true
    (= () true (true) )
    //literal false
    (= () false (false) )
    //literal pair
    (= () p:(Any .Any) (p) )
    //literal ()
    (= () () (()) )

    //end rule stops execution
    (. ((end .Any) Any) -> ({} {}) )
</pre>
In the <a href="https://hlzmn.blogspot.com/2017/12/h-programming-language-static-checking.html" target="_blank">next post</a> I'll talk about the goal of this project which is a robust static checking system for H programs.




