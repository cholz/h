

H Programming Language

    Intro
        a little background
    
    Concatenative
        about stack oriented concatenative languages
        
    H Machine
        state of (program data-stack)
        rules of {rule0, rule1, ...}
        execution of program is 
            loop
                find most specific matching rule
                transform state according to rule
    
    Expressions and Patterns
        Expressions borrowed from Lisp s-expressions
            including integers, symbols (begin with lowercase), and pairs (including list syntax)
        Patterns include expressions
            augmented with Typenames, {options}, [Type constructors]
        user defined types and type constructors
    
    Rules
        include a before pattern and an after pattern
        before part determines if rule matches state
        after part describes what state should look like after firing
        built in rules
            some built in rules have to be implemented by compiler or interpreter because
                they do things other than just transform state that can't be defined in the language
        user defined rules
    
    Static Checking
        most interesting part
        other languages like this (Joy, Forth, Factor) are dynamically checked
            minimal support before runtime for determining program correctness
        uniqueness and ambiguity
        completeness


